import React, {Component} from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFilePdf } from '@fortawesome/free-solid-svg-icons';

class RecordList extends Component{
    render(){
        var link    = 'http://localhost/backend/storage/app/'+this.props.formData.file;
        return(
            <tr >
              <td>
              {this.props.formData.id}
                </td>
                <td>
                    {this.props.formData.nomoragenda}
                </td>
                <td>
                    {this.props.formData.asalsurat}
                </td>
                <td>
                   
                {this.props.formData.data_pengirim == null?"tidak ada pengirim":this.props.formData.data_pengirim.nama }
                </td>
                <td>
                    <a href={link} target="_blank">   <FontAwesomeIcon icon={faFilePdf} className="fawesome"></FontAwesomeIcon></a>
                </td>
                <td>
                    {this.props.formData.updated_at}
                </td>
            </tr>
        ); 
       /* if(this.props.formData.data_pengirim == null){
            return(
                <tr >
                  <td>
                  {this.props.formData.id}
                    </td>
                    <td>
                        {this.props.formData.nomoragenda}
                    </td>
                    <td>
                        {this.props.formData.asalsurat}
                    </td>
                    <td>
                       
                       <p>tidak ada pengirim</p>
                    </td>
                    <td>
                        {this.props.formData.updated_at}
                    </td>
                </tr>
            );  
        }
        else{
            return(
                <tr >
                  <td>
                  {this.props.formData.id}
                    </td>
                    <td>
                        {this.props.formData.nomoragenda}
                    </td>
                    <td>
                        {this.props.formData.asalsurat}
                    </td>
                    <td>
                       
                        {this.props.formData.data_pengirim.nama}
                    </td>
                    <td>
                        {this.props.formData.updated_at}
                    </td>
                </tr>
            );
        }
        */
       
    }
}
export default RecordList;