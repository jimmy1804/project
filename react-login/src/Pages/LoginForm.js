import React, {Component, useState} from 'react';
import axios from 'axios';
import './LForm.css'
import {useNavigate, useParams} from 'react-router-dom';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';


function LoginForm() {

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");


    //Validation
    const [validation, setValidation] = useState([]);

    const navigate = useNavigate();

    const LoginHandler = async (e) => {
        e.preventDefault();

        const formData = new FormData();

        formData.append('username', username);
        formData.append('password', password);
        var url = window.$apiBaseURL+'/post-login';
        var config = {
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json',
            },
        };

        await axios.post(url, formData, config)
            .then((response) => {
                if (response.data.status) {
                    localStorage.setItem('token', response.data.results.token);
                    localStorage.setItem('user_id', response.data.results.user.id);
                    localStorage.setItem('_add', response.data.results.user.divisi.allow_add);
                    localStorage.setItem('_edit', response.data.results.user.divisi.allow_edit);
                    localStorage.setItem('_delete', response.data.results.user.divisi.allow_delete);
                    navigate('/dashboard');
                }
            }).catch((error) => {
                console.log(error, "ini error");
            })


    };


    return (
        <div className='BGBG'>
            <div className="login-container">
                <div className='background'></div>
                <form className="form-login" onSubmit={LoginHandler}>
                    <div className="form-inner">
                        <h3>Login</h3>
                        <div className="form-group">
                            <label htmlFor="username">Username:</label>
                            <Form.Control type="text" id="username" value={username}
                                   onChange={(e) => setUsername(e.target.value)}/>
                            {
                                validation.username && (
                                    <div className="text-danger">
                                        {validation.username[0]}
                                    </div>
                                )
                            }
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Password:</label>
                            <Form.Control type="password" id="password" value={password}
                                   onChange={(e) => setPassword(e.target.value)}/>
                            {
                                validation.password && (
                                    <div className="text-danger">
                                        {validation.password[0]}
                                    </div>
                                )
                            }
                        </div>
                        <Button onSubmit={LoginHandler} type="submit" className="button1">Masuk</Button>
                    </div>


                </form>
            </div>
        </div>
    )
}

export default LoginForm;