import React, {Component} from 'react';
import axios from 'axios';
import RecordList from './RecordList';
import '../App.css'


class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            dsurat: []
        })
    }

    componentDidMount() {
        var token = localStorage.getItem('token');
        var config = {
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json',
                Authorization: `Bearer ` + token
            },
        };
        var url = window.$apiBaseURL + '/get-data';

        axios.get(url, config)
            .then(response => {
                this.setState({dsurat: response.data.results});
            })
            .catch(function (error) {
                console.log(error);
               // window.location.href="/";
            })
    }

    usersList() {
        return this.state.dsurat.map(function (object, index) {
            return <RecordList formData={object} key={index}/>;
        });
    }


    render() {
        return (
            <div>

                <div className='Dashboard'>Dashboard</div>

                <div className='Kmasuk'>
                </div>

                <div className='Kkeluar'>

                </div>

                <div className='Dash'>
                    Kotak Masuk
                    <div className='tabledashboard-container'>
                        <div className='tabledashboard'>
                            <table border="1" cellspacing="0" width="1000px" height="60px" className='desain-tabel'>
                                <thead>
                                <th>No</th>
                                <th>Nomor Agenda</th>
                                <th>Asal Surat</th>
                                <th>Pengirim</th>
                                <th>File</th>
                                <th>Tanggal Kirim</th>

                                </thead>
                                <tbody>
                                {this.usersList()}
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

            </div>
        )
    }


}

export default Dashboard