import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Sidebar from './Component/Sidebar';
import { BrowserRouter as Router, Routes, Route} from 'react-router-dom'
import ArsipS from './Pages_Arsip/ArsipS'
import DArsip from './Pages_Arsip/DArsip'
import HLihat from './Pages_Arsip/HLihat'
import HUnduh from './Pages_Arsip/HUnduh'
import TArsip from './Pages_Arsip/TArsip'

import Dashboard from './Pages/Dashboard';
import WithoutSidebar from './Component/WithoutSidebar';
import WithSidebar from './Component/WithSidebar';
import SuratC from './Pages_Surat/SuratC';
import DSurat from './Pages_Surat/DSurat';
import BEmail from './Pages_Email/BEmail';
import DEmail from './Pages_Email/DEmail';
import JEmail from './Pages_Email/JEmail';
import JEmailt from './Pages_Email/JEmailt';
import SPK from './Pages_Email/SPK';
import SPKADD from './Pages_Email/SPKADD';
import TAkun from './Pages_Admin/TAkun';
import TAkunAd from './Pages_Admin/TAkunAd';
import LoginForm from './Pages/LoginForm';
import DSpk from './Pages_Email/DSpk';
import Kirim from './Pages_Email/Kirim';
import Sperj from './Pages_Email/SPerj';
import DSperj from './Pages_Email/DSperj';
import JEmailTemplate from './Pages_Email/JEmailTemplate';
import JenisEmailTambahTemplate from './Pages_Email/JenisEmailTambahTemplate';
import DSpkAdd from './Pages_Email/DSpkAdd';
import SperjAdd from './Pages_Email/SperjAdd';
import DSperjAdd from './Pages_Email/DSperjAdd';
import EditSpk from './Pages_Email/EditSpk';
import EditSpkAdd from './Pages_Email/EditSpkAdd';
import EditSperj from './Pages_Email/EditSperj';
import EditSperjAdd from './Pages_Email/EditSperjAdd';
import EditPenerimaEmail from './Pages_Email/EditPenerimaEmail';
import MenuAkun from './Pages_Email/MenuAkun';
import EditTemplateEmail from './Pages_Email/EditTemplateEmail';

function App() {
  window.$development = 'production'; //'production' or 'localjimmy'
  window.$apiURLOption  = {
    production:'http://api.jimmy.limentosca.com',

    localjimmy:'http://localhost/backend/public/_api'
  };
  window.$apiBaseURL  = window.$apiURLOption[window.$development];

  return (
    <div className="App">
        <Routes>
          <Route element={<WithoutSidebar/>}>
          <Route path='/' element={<LoginForm/>}/>
          </Route>
          <Route element={<WithSidebar/>}>
          <Route path='/Dashboard' element={<Dashboard/>}/>
          <Route path='/ArsipS' element={<ArsipS/>}/>
          <Route path='/DArsip' element={<DArsip/>}/>
          <Route path='/HLihat' element={<HLihat/>}/>
          <Route path='/HUnduh' element={<HUnduh/>}/>
          <Route path='/SuratC' element={<SuratC/>}/>
          <Route path='/' element={<LoginForm/>}/>
          <Route path='/BEmail' element={<BEmail/>}/>
          <Route path='/SPK' element={<SPK/>}/>
          <Route path='/SPerj' element={<Sperj/>}/>
          <Route path='/Kirim' element={<Kirim/>}/>
          <Route path={'/SPKADD/:id'} element={<SPKADD/>} />
          <Route path={'/EditSpkAdd/:id'} element={<EditSpkAdd/>} />
          <Route path={'/EditSpk/:id'} element={<EditSpk/>} />
          <Route path={'/EditSperj/:id'} element={<EditSperj/>} />
          <Route path={'/EditSperjAdd/:id'} element={<EditSperjAdd/>} />
          <Route path={'/SperjAdd/:id'} element={<SperjAdd/>} />
          <Route path={'/EditPenerimaEmail/:id'} element={<EditPenerimaEmail/>} />
          <Route path={'/EditTemplateEmail/:id'} element={<EditTemplateEmail/>} />
          <Route path='/DEmail' element={<DEmail/>}/>
          <Route path='/DSpk' element={<DSpk/>}/>
          <Route path='/MenuAkun' element={<MenuAkun/>}/>      
          <Route path='/DSpkAdd' element={<DSpkAdd/>}/>
          <Route path='/DSperj' element={<DSperj/>}/>
          <Route path='/DSperjAdd' element={<DSperjAdd/>}/>
          <Route path='/JEmail' element={<JEmail/>}/>
          <Route path='/JEmailTemplate' element={<JEmailTemplate/>}/>
          <Route path='/JEmailt' element={<JEmailt/>}/>
          <Route path='/JenisEmailTambahTemplate' element={<JenisEmailTambahTemplate/>}/>
          </Route>
        </Routes>
    </div>
  );
}

export default App;
