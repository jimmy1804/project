import React, {Component, useEffect, useState} from 'react';
import axios from 'axios';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import ReactTags from 'react-tag-autocomplete';

function BEmail() {
   
    const [file, setFile] = useState("");

    const [tags, setTags] = useState([]);
    const [suggestions, setSuggestions] = useState([]);
    const [emailtemplate, setemailtemplate] = useState([]);
    const [opendiv, setOpenDiv] = useState(false);
    const [validation, setValidation] = useState([]);
    const [judulemail, setJudulEmail]   = useState("");
    const [isiemail, setIsiEmail]   = useState("");

    useEffect(() => {
        async function fetch() {
            var urlGetEmailAndTemplate = window.$apiBaseURL + '/get-dataemail-and-template';
            var token = localStorage.getItem('token');
            var config = {
                headers: {
                    'Content-type': 'application/json',
                    'Accept': 'application/json',
                    Authorization: `Bearer `+token
                },
            };

            await axios.get(urlGetEmailAndTemplate, config).then(response => {
                if (response.data.status) {
                    setSuggestions(response.data.results.email);
                    setemailtemplate(response.data.results.template);
                } else {
                    setSuggestions([]);
                }
            }).catch(function (error) {
                window.location.href    = '/';
                return [];
            });

        }

        fetch();
    }, []);


    function onDelete(i) {
        const tempTags = tags.slice(0);
        tempTags.splice(i, 1);
        setTags(tempTags);
    }

    function onAddition(tag) {
        tags.concat(tag);
        setTags(tags.concat(tag));
    }


    const SubmitHandler = async (e) => {
        e.preventDefault();
        var penerimaemailstring = tags.map(function(elem){
            return elem.id;
        }).join(";");

        const formData = new FormData();

        
        formData.append('user_id', localStorage.getItem('user_id'));
        formData.append('file', file);
        formData.append('user_username', localStorage.getItem('user_username'));
        formData.append('judul_email', judulemail);
        formData.append('isi_email', isiemail);
        formData.append('penerima_email', penerimaemailstring);


        var token = localStorage.getItem('token');
        var url = window.$apiBaseURL + '/post-composespk';

        await axios.post(url, formData).then((response) => {
            alert("berhasil");
            if (response.data.status) {
            }
        }).catch((error) => {
            console.log(error.response.data);
        });
    };

    function onChangeEmailTemplate(e) {
        var id      = e.target.value;
        var found   = emailtemplate.find(x => x.id == id);
        if(found) {
            setIsiEmail(found.isi_email);
            setJudulEmail(found.title);
        }
    }

    return (
        <div>
            <div className='SPK'>SPK</div>
            <div className='FormSPK'>
                <Form>
                    <table>
                        <tbody>
                        
                       
                        <tr>
                            <td>Tujuan Email</td>
                            <td>
                                <ReactTags
                                    tags={tags}
                                    suggestions={suggestions}
                                    onDelete={onDelete.bind(this)}
                                    placeholderText="Silahkan masukkan email"
                                    onAddition={onAddition.bind(this)}/>
                            </td>
                        </tr>
                        <tr>
                            <td>Template Email</td>
                            <td>
                                <Form.Select onChange={onChangeEmailTemplate.bind(this)}>
                                    <option>Pilih template email</option>
                                    {emailtemplate.map((item, index) => (
                                        <option key={index} value={item.id}>
                                            {item.judul_template}
                                        </option>
                                    ))}
                                </Form.Select>
                            </td>
                        </tr>
                        <tr>
                            <td>Judul Email</td>
                            <td><Form.Control type="text" id="judulemail" value={judulemail} onChange={(e) => setJudulEmail(e.target.value)} /></td>
                        </tr>
                        <tr>
                            <td width="300">
                                Isi Email
                                <br />
                                <code>
                                    contoh:
                                    gunakan &#123;nama_penerima&#125; untuk menghasilkan nama penerima
                                </code>
                            </td>
                            <td>
                                <Form.Control as="textarea" rows="5" cols="50" id="isiemail" value={isiemail} onChange={(e) => setIsiEmail(e.target.value)}/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>Lampiran</p>
                            </td>
                            <td>
                                <Form.Control type="file" name="upload" id='file' multiple
                                              onChange={(e) => setFile(e.target.files[0])}/></td>
                        </tr>
                        <tr>
                            <td>
                                <Button onClick={SubmitHandler}>Submit</Button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </Form>

            </div>
        </div>
    )
}

export default BEmail