import React, {Component} from 'react';
import axios from 'axios';
import {
    BrowserRouter as Router,
    Link,
    Route,
    Routes,
    useNavigate,
    useParams,
} from "react-router-dom";

class EditSpkAdd extends Component {
    

    constructor(props) {

        super(props);
        this.onChangefile = this.onChangefile.bind(this);
        this.state = ({
        
            file: '',
            dataspk: []

        })

        this.handleClick = this.handleClick.bind(this);
    }


    onChangefile(e) {
        let files = e.target.files;
        // console.log(files[0]);
        // this.setState({ file: files[0] }, () => { console.log(this.state.files) });
        this.setState({
            file: files[0]
        });
    }


    componentDidMount() {
        var token = localStorage.getItem('token');
        var config = {
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json',
                Authorization: `Bearer ` + token
            },
        };
        var url = window.$apiBaseURL + '/get-dataspk-add/' + this.props.params.id;
        axios.get(url, config).then(response => {
            console.log(response.data);
            this.setState({dataspk: response.data.results});
        })
            .catch(function (error) {
                console.log(error);
            })
    }

    handleClick() {

        var dataspk = this.state.dataspk;
        const formData = new FormData();

        
        formData.append('nomorspk', dataspk.nomorspk);
        formData.append('pic', dataspk.pic);
        formData.append('noaddspk', dataspk.noaddspk);
        formData.append('tgladdspk', dataspk.tgladdspk);
        formData.append('namavendor', dataspk.namavendor);
        formData.append('perwakilan', dataspk.perwakilan);
        formData.append('perihal', dataspk.perihal);
        formData.append('perubahan_tambahan_spk', dataspk.perubahan_tambahan_spk);
        formData.append('perubahan_tambahan_spk2', dataspk.perubahan_tambahan_spk2);
        formData.append('tanggalmulai', dataspk.tanggalmulai);
        formData.append('tanggalselesai', dataspk.tanggalselesai);
        formData.append('file', this.state.file);


        var token = localStorage.getItem('token');
        var config = {
            headers: {Authorization: `Bearer `+token}
        };
        const data_spkadd = this.props.params.id
        var url = window.$apiBaseURL + `/post-dataspk-add/${data_spkadd}`;

        axios.post(url, formData, config).then((response) => {
            console.log(response.data);
            alert("berhasil");
            if (response.data.status) {
            window.location.href="DSpkAdd";
            }
        }).catch((error) => {
            window.location.href="/";
            console.log(error.response.data);
        });

/*
        var token = localStorage.getItem('token');
        var config = {
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json',
                Authorization: `Bearer ` + token
            },
        };
        var url = window.$apiBaseURL + '/post-composespk-adamdum';


        axios.post(url, config,formData).then((response) => {
            // console.log(response);
            // alert("berhasil");
            if (response.data.status) {


            }
        }).catch((error) => {
            console.log(error.response.data);
          
            // console.log(error, "ini error");
        });
*/
    }

    render() {
        var _edit = localStorage.getItem('_edit');
        if(_edit == 0) {
            window.location.href='/DSpkAdd';
            return;
        } else {

            var dataspk = this.state.dataspk;

            return (
                <div>
                    <div className='SPKADD'>SPK ADD untuk surat id ke</div>
                    <div className='FormSPKADD'>
                        <table>
                        <tr>
                                <td>PIC</td>

                                <td><input type="text" id="pic" value={dataspk.pic}
                                           onChange={this.handleChange}/></td>
                            </tr>
                            <tr>
                                <td>SPK Awal</td>
                                
                                <td><input type="text" id="nomorspk" value={dataspk.nomorspk}
                                           onChange={this.handleChange}/></td>
                                
                            </tr>
                            <tr>
                                <td>No.ADD SPK</td>
                                <td><input type="text" id="noaddspk" value={dataspk.noaddspk}
                                           onChange={this.handleChange}/></td>
                            </tr>
                            <tr>
                                <td>Tgl ADD SPK</td>
                                <td><input type="text" id="tgaddspk" value={dataspk.tgladdspk}
                                           onChange={this.handleChange}/></td>
                            </tr>
                            <tr>
                                <td>Nama Vendor / Kontraktor</td>

                                <td><input type="text" id="namavendor" value={dataspk.namavendor}
                                           onChange={this.handleChange}/></td>
                            </tr>
                            <tr>
                                <td>Perwakilan (dasar kewewenangan)</td>
                                <td><input type="text" id="perwakilan" value={dataspk.perwakilan}
                                           onChange={this.handleChange}/></td>
                            </tr>
                            <tr>
                                <td>Perihal</td>
                                <td><input type="text" id="perihal" value={dataspk.perihal}
                                           onChange={this.handleChange}/></td>
                            </tr>
                            <tr>
                                <td>Perubahan/Tambahan SPK</td>
                                <td><input type="text" id="perubahan_tambahan_spk" value={dataspk.perubahan_tambahan_spk}
                                           onChange={this.handleChange}/></td>
                            </tr>
                            <tr>
                                <td>Perubahan/Tambahan SPK</td>
                                <td><input type="text" id="perubahan_tambahan_spk2" value={dataspk.perubahan_tambahan_spk2}
                                           onChange={this.handleChange}/></td>
                            </tr>
                            <tr>
                                <td>Tanggal Mulai</td>
                                <td><input type="text" id="tanggalmulai" value={dataspk.tanggalmulai}
                                           onChange={this.handleChange}/></td>
                            </tr>
                            <tr>
                                <td>Tanggal Selesai</td>
                                <td><input type="text" id="tanggalselesai" value={dataspk.tanggalselesai}
                                           onChange={this.handleChange}/></td>
                            </tr>
                            <tr>
                                <td>Keterangan</td>
                                <td><textarea id="keterangan" value={dataspk.keterangan}
                                              onChange={this.handleChange}></textarea></td>
                            </tr>
                            <div className='lampiran1'>
                                <p>Lampiran</p>
                                <tr>
                                    <td>
                                        <input type="file" name="berkas" onChange={this.onChangefile}/>
                                    </td>
                                </tr>
                            </div>
                            <tr>
                                <td><input type="submit" value="Submit" onClick={this.handleClick}/></td>
                            </tr>
                        </table>

                    </div>
                </div>
            )
        }
    }

    handleChange = (event) => {
        const {id, value} = event.target;
        this.setState((prevState) => ({
            dataspk: {
                ...prevState.dataspk,
                [id]: value
            }
        }));
    };

}

export default (props) => {
    const params = useParams();

  
  

    return (
        <EditSpkAdd {...props} params={params}/>
    );
}
