import React, {Component} from "react";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faFilePdf, faPenToSquare, faTrash} from '@fortawesome/free-solid-svg-icons';
import axios from "axios";
import '../App.css'

class RecordSpk extends Component {
    onClickDelete(e) {
        e.preventDefault();
        var id = e.currentTarget.dataset.id;

        const formData = new FormData();

        formData.append('id', id);
        var token = localStorage.getItem('token');
        var config = {
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json',
                Authorization: `Bearer ` + token
            },
        };
        var url = window.$apiBaseURL + '/delete-dataspk';
        axios.post(url, formData, config).then((response) => {
            if (response.data.status) {
                window.location.reload()
            } else console.log(response.data.message);
        }).catch((error) => {
            console.log(error.response.data);
            window.location.href = "/";
        });
    }

    render() {
        var link = 'http://localhost/backend/storage/app/' + this.props.formData.file;
        var edit = 'http://localhost:3000/SPKADD/' + this.props.formData.id;
        var eedit = 'http://localhost:3000/EditSpk/' + this.props.formData.id;
        var _editspk = localStorage.getItem('_edit');
        var _edit = localStorage.getItem('_edit');
        var _delete = localStorage.getItem('_delete');

        return (
            
            <tr>
                <td>
                    {this.props.formData.id}
                </td>
                <td>
                    {this.props.formData.pic}
                </td>
                <td>
                    {this.props.formData.suratpenunjukan}
                </td>
                <td>
                    {this.props.formData.nomorspk}
                </td>
                <td>
                    {this.props.formData.tanggalspk}
                </td>
                <td>
                    {this.props.formData.namavendor}

                </td>
                <td>
                    {this.props.formData.perwakilan}
                </td>
                <td>
                    {this.props.formData.pekerjaan}
                </td>
                <td>
                    {this.props.formData.nilaispk}
                </td>
                <td>
                    {this.props.formData.pajak}

                </td>
                <td>
                    {this.props.formData.tanggalmulai}
                </td>
                <td>
                    {this.props.formData.tanggalselesai}

                </td>
                <td>
                    <a href={link} target="_blank"> <FontAwesomeIcon icon={faFilePdf} size="2x"
                                                                     className="fawesome"></FontAwesomeIcon></a>
                </td>
                <td>

                </td>
                <td>
                      {_edit == 0 ? '' : <a href={eedit}><FontAwesomeIcon
                        icon={faPenToSquare} size="2x" className="fawesome"></FontAwesomeIcon></a>}    
                 
                    {_delete == 1 ? <a href="#" data-id={this.props.formData.id} onClick={this.onClickDelete}><FontAwesomeIcon
                        icon={faTrash} size="2x" className="fawesome"></FontAwesomeIcon></a> : ''}
                </td>
                <td>
                    {_edit == 0 ? '' : <a href={edit}><FontAwesomeIcon
                        icon={faPenToSquare} size="2x" className="fawesome"></FontAwesomeIcon></a>}
                    
                </td>
            </tr>
        );
        /* if(this.props.formData.data_pengirim == null){
             return(
                 <tr >
                   <td>
                   {this.props.formData.id}
                     </td>
                     <td>
                         {this.props.formData.nomoragenda}
                     </td>
                     <td>
                         {this.props.formData.asalsurat}
                     </td>
                     <td>

                        <p>tidak ada pengirim</p>
                     </td>
                     <td>
                         {this.props.formData.updated_at}
                     </td>
                 </tr>
             );
         }
         else{
             return(
                 <tr >
                   <td>
                   {this.props.formData.id}
                     </td>
                     <td>
                         {this.props.formData.nomoragenda}
                     </td>
                     <td>
                         {this.props.formData.asalsurat}
                     </td>
                     <td>

                         {this.props.formData.data_pengirim.nama}
                     </td>
                     <td>
                         {this.props.formData.updated_at}
                     </td>
                 </tr>
             );
         }
         */

    }
}

export default RecordSpk;