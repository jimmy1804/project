import React, { Component, useEffect, useState } from 'react';
import axios from 'axios';

function Kirim() {
    const [nomoragenda, setNomoragenda] = useState("");
    const [asalsurat, setAsalsurat] = useState("");
    const [file,setFile] = useState("");
  

    const [validation, setValidation] = useState([]);


  

    const SubmitHandler = async (e) => {
        e.preventDefault();
    
        const formData = new FormData();
    
        formData.append('nomoragenda',nomoragenda);
        formData.append('asalsurat',asalsurat);
        formData.append('user_id', localStorage.getItem('user_id'));
        formData.append('file', file);
        formData.append('user_username', localStorage.getItem('user_username'));
        
      
        var token = localStorage.getItem('token');
        var url     = window.$apiBaseURL + '/post-compose';
        await axios.post(url, formData).
        then((response) => {
          console.log(response.data);
          alert("berhasil");
          // localStorage.getItem('token', response.data.results.token);
          
          if(response.data.status){
           // localStorage.setItem('token', response.data.token);
            
          }
            //console.log(response.data.token);
            // localStorage.setItem('token', response.data.token);
            //console.log("berhasil");
            // navigate('/Dashboard');
        }).catch((error) => {
            console.log(error.response.data);
           // console.log(error, "ini error");
        });
    
    
      };


  return (
    <div>
        <div className='SPK'>Pengiriman file</div>
        <div className='FormSPK'>
       
        <table>
            <tr>
                <td>Nomor Agenda</td>
                <td><input type="text" id="nomoragenda" value={nomoragenda} 
                 onChange={(e) => setNomoragenda(e.target.value)} /></td>
            </tr>
            
             
            <tr>
                <td>Asal Surat</td>
                <td><input type="text" id="asalsurat" value={asalsurat} 
                 onChange={(e) => setAsalsurat(e.target.value)}/></td>
            </tr>
            <tr>
                <td>Tujuan Surat</td>
                <td><input type="text" id="pengirim"/></td>
            </tr>
           

            <div className='lampiran1'>
            <p>Lampiran</p>
            <tr> 
            <td><input type="file" name="upload" id='file'   
            onChange={(e) => setFile(e.target.files[0])} /> </td>
            </tr>
            </div>
            <tr>
                <td><button onClick={SubmitHandler} >submit</button></td>
            </tr>
        </table>
     
        
        
        </div>  
    </div>
  )
}

export default Kirim