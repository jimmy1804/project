import React, {Component, useEffect, useState} from 'react';
import axios from 'axios';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import ReactTags from 'react-tag-autocomplete';

function MenuAkun() {
    const [password, setPassword] = useState("");
    const [newpassword, setNewPassword] = useState("");

 
    const [validation, setValidation] = useState([]);
   
    const Logout = async (e) => {

        const formData = new FormData();

       
        var token = localStorage.getItem('token');
        var config = {
            headers: {Authorization: `Bearer `+token}
        };
        var url = window.$apiBaseURL + '/post-logout';

        await axios.post(url, formData, config).then((response) => {
            if (response.data.status) {
                console.log(response.data);
                window.location.href = "/";
            }
        }).catch((error) => {
            window.location.reload()
            console.log(error.response.data);
        });
    };



    const SubmitHandler = async (e) => {

        const formData = new FormData();
        formData.append('password', password);
        formData.append('newpassword', newpassword);
       
        var token = localStorage.getItem('token');
        var config = {
            headers: {Authorization: `Bearer `+token}
        };
        var url = window.$apiBaseURL + '/post-password';

        await axios.post(url, formData, config).then((response) => {
            alert("berhasil");
            if (response.data.status) {
                console.log(response.data);
              //  window.location.href="DSpk";
            }
        }).catch((error) => {
            //window.location.href="/";
            console.log(error.response.data);
        });
    };


    return (
        <div>
            <div className='SPK'>Menu Akun</div>
            <div className='FormSPK'>
                <Form>
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                Password Baru
    
                            </td>
                            <td>
                                <Form.Group className="mb-3">
                                    <Form.Control type="password" id="pic" value={password}
                                                  onChange={(e) => setPassword(e.target.value)}/>
                                </Form.Group>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Konfirmasi Password
                            </td>
                            <td>
                                <Form.Group className="mb-3">
                                    <Form.Control type="password" id="suratpenunjukna" value={newpassword}
                                                  onChange={(e) => setNewPassword(e.target.value)}/>
                                </Form.Group>
                            </td>
                        </tr>
    
                        <tr>
                            <td>
                                <Button onClick={SubmitHandler}>Submit</Button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </Form>
                <hr></hr>
                <br></br>
                <div className='clicklogout'>    <td>
                                <Button onClick={Logout}>Logout</Button>
                            </td></div>
            
            </div>
        </div>
    )
}

export default MenuAkun