import React, {Component} from 'react';
import axios from 'axios';
import {
    BrowserRouter as Router,
    Link,
    Route,
    Routes,
    useNavigate,
    useParams,
} from "react-router-dom";

class EditSpk extends Component {
    

    constructor(props) {
        

        super(props);
        this.onChangefile = this.onChangefile.bind(this);
        this.state = ({
            file: '',
            dataspk: []

        })

        this.handleClick = this.handleClick.bind(this);
    }
    onChangefile(e) {
        let files = e.target.files;
        // console.log(files[0]);
        // this.setState({ file: files[0] }, () => { console.log(this.state.files) });
        this.setState({
            file: files[0]
        });
    }

   


    componentDidMount() {
      
        var token = localStorage.getItem('token');
        var config = {
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json',
                Authorization: `Bearer ` + token
            },
        };
        var url = window.$apiBaseURL + '/get-dataspk/' + this.props.params.id;
        axios.get(url, config).then(response => {
           
            this.setState({dataspk: response.data.results});
        })
            .catch(function (error) {
                console.log(error);
            })
    }

    handleClick() {

        var dataspk = this.state.dataspk;
        const formData = new FormData();

        formData.append('pic', dataspk.pic);
        formData.append('nomorspk', dataspk.nomorspk);
        formData.append('tanggalspk', dataspk.tanggalspk);
        formData.append('namavendor', dataspk.namavendor);
        formData.append('perwakilan', dataspk.perwakilan);
        formData.append('pekerjaan', dataspk.pekerjaan);
        formData.append('nilaispk', dataspk.nilaispk);
        formData.append('pajak', dataspk.pajak);
        formData.append('tanggalmulai', dataspk.tanggalmulai);
        formData.append('tanggalselesai', dataspk.tanggalselesai);
        formData.append('keterangan', dataspk.keterangan);
        formData.append('file', this.state.file);


        var token = localStorage.getItem('token');
        var config = {
            headers: {Authorization: `Bearer `+token}
        };
        const data_spk = this.props.params.id
        var url = window.$apiBaseURL + `/post-dataspk/${data_spk}`;

        axios.post(url, formData, config).then((response) => {
            alert("berhasil");
            if (response.data.status) {
                window.location.href="/DSpk";
            }
        }).catch((error) => {
            window.location.href="/";
            console.log(error.response.data);
        });

/*
        var token = localStorage.getItem('token');
        var config = {
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json',
                Authorization: `Bearer ` + token
            },
        };
        var url = window.$apiBaseURL + '/post-composespk-adamdum';


        axios.post(url, config,formData).then((response) => {
            // console.log(response);
            // alert("berhasil");
            if (response.data.status) {


            }
        }).catch((error) => {
            console.log(error.response.data);
          
            // console.log(error, "ini error");
        });
*/
    }

    render() {
        var _edit = localStorage.getItem('_edit');
        if(_edit == 0) {
            window.location.href='/DSpk';
            return;
        } else {

            var dataspk = this.state.dataspk;

            return (
                <div>
                    <div className='SPKADD'>SPK</div>
                    <div className='FormSPKADD'>
                        <table>
                        <tr>
                                <td>PIC</td>

                                <td><input type="text" id="pic" value={dataspk.pic}
                                           onChange={this.handleChange}/></td>
                            </tr>
                            <tr>
                                <td>Nomor SPK</td>
                                
                                <td><input type="text" id="nomorspk" value={dataspk.nomorspk}
                                           onChange={this.handleChange}/></td>
                                
                            </tr>
                            <tr>
                                <td>Tanggal SPK</td>
                            
                               <td><input type="text" id="tanggalspk" value={dataspk.tanggalspk}
                                           onChange={this.handleChange}/></td>
                            </tr>
                            <tr>
                                <td>Nama Vendor / Kontraktor</td>

                                <td><input type="text" id="namavendor" value={dataspk.namavendor}
                                           onChange={this.handleChange}/></td>
                            </tr>
                            <tr>
                                <td>Perwakilan (dasar kewewenangan)</td>
                                <td><input type="text" id="perwakilan" value={dataspk.perwakilan}
                                           onChange={this.handleChange}/></td>
                            </tr>
                            <tr>
                                <td>Pekerjaan</td>
                               <td><input type="text" id="pekerjaan" value={dataspk.pekerjaan}
                                           onChange={this.handleChange}/></td>
                            </tr>
                            <tr>
                                <td>Nilai SPK</td>
                                <td><input type="text" id="nilaispk" value={dataspk.nilaispk}
                                           onChange={this.handleChange}/></td>
                            </tr>
                            <tr>
                                <td>Pajak</td>
                                <td><input type="text" id="pajak" value={dataspk.pajak}
                                           onChange={this.handleChange}/></td>
                            </tr>
                            <tr>
                                <td>Tanggal Mulai</td>
                                <td><input type="text" id="tanggalmulai" value={dataspk.tanggalmulai}
                                           onChange={this.handleChange}/></td>
                            </tr>
                            <tr>
                                <td>Tanggal Selesai</td>
                                <td><input type="text" id="tanggalselesai" value={dataspk.tanggalselesai}
                                           onChange={this.handleChange}/></td>
                            </tr>
                            <tr>
                                <td>Keterangan</td>
                                <td><textarea id="keterangan" value={dataspk.keterangan}
                                              onChange={this.handleChange}></textarea></td>
                            </tr>
                            <div className='lampiran1'>
                                <p>Lampiran</p>
                                <tr>
                                    <td>
                                    <input type="file" name="berkas" onChange={this.onChangefile}/>
                                    </td>
                                </tr>
                            </div>
                            <tr>
                                <td><input type="submit" value="Submit" onClick={this.handleClick}/></td>
                            </tr>
                        </table>

                    </div>
                </div>
            )
        }
    }

    handleChange = (event) => {
        const {id, value} = event.target;
        this.setState((prevState) => ({
            dataspk: {
                ...prevState.dataspk,
                [id]: value
            }
        }));
    };

}

export default (props) => {
    const params = useParams();

  
  

    return (
        <EditSpk {...props} params={params}/>
    );
}
