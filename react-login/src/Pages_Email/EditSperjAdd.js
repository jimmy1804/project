import React, {Component} from 'react';
import axios from 'axios';
import Form from 'react-bootstrap/Form';
import {
    BrowserRouter as Router,
    Link,
    Route,
    Routes,
    useNavigate,
    useParams,
} from "react-router-dom";

class EditSperjAdd extends Component {
    

    constructor(props) {

        super(props);
        this.onChangefile = this.onChangefile.bind(this);
        this.state = ({
        
            file: '',
            datasperj: []

        })

        this.handleClick = this.handleClick.bind(this);
    }


    onChangefile(e) {
        let files = e.target.files;
        // console.log(files[0]);
        // this.setState({ file: files[0] }, () => { console.log(this.state.files) });
        this.setState({
            file: files[0]
        });
    }


    componentDidMount() {
        var token = localStorage.getItem('token');
        var config = {
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json',
                Authorization: `Bearer ` + token
            },
        };
        var url = window.$apiBaseURL + '/get-datasperj-add/' + this.props.params.id;
        axios.get(url, config).then(response => {
            console.log(response.data);
            this.setState({datasperj: response.data.results});
        })
            .catch(function (error) {
                console.log(error);
            })
    }

    handleClick() {

        var datasperj = this.state.datasperj;
        const formData = new FormData();

        
     
        formData.append('pic', datasperj.pic);
        formData.append('nomorsperjadd', datasperj.nomorsperjadd);
        formData.append('add_atas_nosperj', datasperj.add_atas_nosperj);
        formData.append('tgladd', datasperj.tgladd);
        formData.append('namavendor', datasperj.namavendor);
        formData.append('perwakilan', datasperj.perwakilan);
        formData.append('perihal', datasperj.perihal);
        formData.append('perubahan', datasperj.perubahan);
        formData.append('pajak', datasperj.pajak);
        formData.append('tglmulai', datasperj.tglmulai);
        formData.append('tglselesai', datasperj.tglselesai);
        formData.append('file', this.state.file);
        formData.append('keterangan', datasperj.keterangan);



        var token = localStorage.getItem('token');
        var config = {
            headers: {Authorization: `Bearer `+token}
        };
        const data_sperjadd = this.props.params.id
        var url = window.$apiBaseURL + `/post-datasperj-add/${data_sperjadd}`;

        axios.post(url, formData, config).then((response) => {
            console.log(response.data);
            alert("berhasil");
            if (response.data.status) {
            window.location.href="DSpkAdd";
            }
        }).catch((error) => {
           // window.location.href="/";
            console.log(error.response.data);
        });

/*
        var token = localStorage.getItem('token');
        var config = {
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json',
                Authorization: `Bearer ` + token
            },
        };
        var url = window.$apiBaseURL + '/post-composespk-adamdum';


        axios.post(url, config,formData).then((response) => {
            // console.log(response);
            // alert("berhasil");
            if (response.data.status) {


            }
        }).catch((error) => {
            console.log(error.response.data);
          
            // console.log(error, "ini error");
        });
*/
    }

    render() {
        var _edit = localStorage.getItem('_edit');
        if(_edit == 0) {
            window.location.href='/DSperjAdd';
            return;
        } else {

            var datasperj = this.state.datasperj;

            return (
                <div>
                    <div className='SPKADD'>Edit Sperj Add</div>
                    <div className='FormSPKADD'>
                        <table>
                        <tr>
                            <td>
                                PIC
                                <br />
                              
                            </td>
                            <td>
                                <Form.Group className="mb-3">
                                    <Form.Control type="text" id="pic" value={datasperj.pic}
                                                  onChange={this.handleChange}/>
                                </Form.Group>
                            </td>
                        </tr>
                    <tr>
                        <td>Nomor SPERJ ADD</td>
                        <td><Form.Control type="text" id="nomorsperjadd" value={datasperj.nomorsperjadd}
                                         onChange={this.handleChange}/></td>
                    </tr>
                    <tr>
                        <td>Addendum atas SPERJ No.</td>
                        <td><Form.Control type="text" id="add_atas_nosperj" value={datasperj.add_atas_nosperj}
                                          onChange={this.handleChange}/></td>
                    </tr>
                    <tr>
                        <td>Tanggal Add</td>
                        <td><Form.Control type="date" id="tgladd" value={datasperj.tgladd}
                                          onChange={this.handleChange}/></td>
                    </tr>
                    <tr>
                        <td>Nama Vendor / Pihak Kerja Sama</td>
                        <td><Form.Control type="text" id="namavendor" value={datasperj.namavendor}
                                          onChange={this.handleChange}/></td>
                    </tr>
                    <tr>
                        <td>Perwakilan</td>
                        <td><Form.Control type="text" id="perwakilan" value={datasperj.perwakilan}
                                         onChange={this.handleChange}/></td>
                    </tr>
                    <tr>
                        <td>Perihal</td>
                        <td><Form.Control type="text" id="perihal" value={datasperj.perihal}
                                         onChange={this.handleChange}/></td>
                    </tr>
                    <tr>
                        <td>Perubahan</td>
                        <td><Form.Control type="text" id="perubahan" value={datasperj.perubahan}
                                          onChange={this.handleChange}/></td>
                    </tr>
                 
                    <tr>
                        <td>Pajak</td>
                        <td><Form.Control type="text" id="pajak" value={datasperj.pajak}
                                        onChange={this.handleChange}/></td>
                    </tr>
                    <tr>
                        <td>Tanggal Mulai</td>
                        <td><Form.Control type="date" id="tglmulai" value={datasperj.tglmulai}
                                         onChange={this.handleChange}/></td>
                    </tr>
                    <tr>
                        <td>Tanggal Selesai</td>
                        <td><Form.Control type="date" id="tglselesai" value={datasperj.tglselesai}
                                         onChange={this.handleChange}/></td>
                    </tr>
                    <tr>
                        <td>Keterangan</td>
                        <td><textarea id="keterangan" value={datasperj.keterangan}
                                              onChange={this.handleChange}></textarea></td>
                    </tr>
                    <tr>
                        <td>Lampiran</td>
                        <td>
                                        <Form.Control type="file" name="berkas" onChange={this.onChangefile}/>
                                    </td>
                    </tr>
                    <tr>
                    <td><input type="submit" value="Submit" onClick={this.handleClick}/></td>
                    </tr>
                        </table>

                    </div>
                </div>
            )
        }
    }

    handleChange = (event) => {
        const {id, value} = event.target;
        this.setState((prevState) => ({
            datasperj: {
                ...prevState.datasperj,
                [id]: value
            }
        }));
    };

}

export default (props) => {
    const params = useParams();

  
  

    return (
        <EditSperjAdd {...props} params={params}/>
    );
}
