import React, {Component, useState} from 'react';
import axios from 'axios';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

function JEmailt() {
    const [namapenerima, setNamaPenerima] = useState("");
    const [penerimaemail, setPenerimaEmail] = useState("");
    const [gender, setGender] = useState("Pria","Wanita");
    var _add = localStorage.getItem('_add');

    const SubmitHandler = async (e) => {
        e.preventDefault();

        if(_add == 1) {
            const formData = new FormData();
            formData.append('namapenerima', namapenerima);
            formData.append('penerimaemail', penerimaemail);
            formData.append('gender', gender);

            formData.append('user_username', localStorage.getItem('user_username'));

            var url = window.$apiBaseURL + '/post-composeemail';
            var token = localStorage.getItem('token');
            var config = {
                headers: {Authorization: `Bearer ` + token}
            };
            await axios.post(url, formData, config).then((response) => {
                alert("berhasil");

                if (response.data.status) {
                    window.location.href = "/JEmail";
                }
            }).catch((error) => {
                window.location.href = "/";
                console.log(error.response.data);
            });
        } else {
            window.location.href="JEmail";
        }
    };

    if(_add == 0) {
        window.location.href="JEmail";
        return;
    } else {
        return (
            <div>
                <div className='JEmailt'>Buat Email</div>
                <div className='Jenis-Email'>
                    <table>
                        <tr>
                            <td>Nama Penerima</td>
                            <td><Form.Control type="text" id="namapenerima" value={namapenerima}
                                              onChange={(e) => setNamaPenerima(e.target.value)}/></td>
                        </tr>
                        <tr>
                            <td>Penerima Email</td>
                            <td><Form.Control type="email" id="penerimaemail" value={penerimaemail}
                                              onChange={(e) => setPenerimaEmail(e.target.value)}/></td>
                        </tr>
                        <tr>
                            <td>Gender</td>
                            <td><Form.Check type="radio" id="gender" value={gender=='Pria'}
                                            onClick={() => setGender('Pria')}/>Pria</td>
                            <td><Form.Check type="radio" id="gender" value={gender=="Wanita"}
                                            onClick={() => setGender('Wanita')}/>Wanita</td>

                        </tr>

                        <tr>
                            <td>
                                <Button onClick={SubmitHandler}>submit</Button>
                            </td>
                        </tr>
                    </table>

                </div>
            </div>
        )
    }
}

export default JEmailt