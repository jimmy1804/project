import React, {Component, useState} from 'react';
import axios from 'axios';
import RecordSpk from './RecordSpk';
import '../App.css';
import '../table.css';

class DSpk extends Component {
    constructor(props) {
        super(props);
        this.state = ({
         dataspk : []
        });
    }

    getRequest = async (login) => {
        var url = window.$apiBaseURL + '/get-dataspk';
        var token = localStorage.getItem('token');
        var config = {
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json',
                Authorization: `Bearer ` + token
            },
        };

        await axios.get(url, config).then(response => {
            this.setState({dataspk: response.data.results});
        }).catch(function (error) {
            window.location.href = "/";
        })

    }

    componentDidMount() {
        this.getRequest(this.state.login);
    }

    usersList() {
        return this.state.dataspk.map(function (object, index) {
            return <RecordSpk formData={object} key={index}/>;
        });
    }

    render() {

        return (
            <div>
                <div className='DEmail'>Data SPK</div>

                <div className='Search1'>
                    <table>
                        <tr>
                            <td><input type="text" id="searchnomor" placeholder="Cari Surat"/></td>
                            <td><input type="submit" value="Search"/></td>
                        </tr>
                    </table>

                </div>
                <div className='tableemail-container'>
                    <div className='tabledashboard'>
                        <table border="1" cellspacing="1" width="1200px" height="60px">
                            <thead>
                            <th>No</th>
                            <th>PIC</th>
                            <th>Surat Penunjukan</th>
                            <th>Nomor SPK</th>
                            <th>Tanggal SPK</th>
                            <th>Nama Vendor</th>
                            <th>Perwakilan</th>
                            <th width="500px">Pekerjaan</th>
                            <th>Nilai SPK</th>
                            <th>Pajak</th>
                            <th>Tanggal Mulai</th>
                            <th>Tanggal Selesai</th>
                            <th>File</th>
                            <th>Keterangan</th>
                            <th>Action</th>
                            <th>ADD</th>

                            </thead>
                            <tbody className='atur'>
                            {this.usersList()}
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

        )
    }


}

export default DSpk;
