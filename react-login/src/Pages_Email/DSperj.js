import React, { Component} from 'react';
import axios from 'axios';
import '../App.css'
import RecordSperj from './RecordSperj';

class DSperj extends Component {
    constructor(props) {
      super(props);
      this.state = ({
        datasperj:[]
      })
    }
    componentDidMount() {
        var url = window.$apiBaseURL+'/get-datasperj';
        var token = localStorage.getItem('token');
            var config = {
                headers: {Authorization: `Bearer `+token}
            };
      
      axios.get(url,config).then(response => {
        this.setState({ datasperj: response.data.results});
      }).catch(function (error) {
        console.log(error);
      })
    }    
    
    usersList(){
      // console.log("asd");
      // console.log(this.state.dsurat);
      
        return this.state.datasperj.map(function(object, index){
          return <RecordSperj formData={object} key={index} />;
        });
      // console.log(data);

      
    }
 

    render(){
      return (
        <div>
        <div className='DEmail'>Data SPerj</div>

        <div className='Search1'>
        <table>
            <tr>
                <td><input type="text" id="searchnomor" placeholder="Cari Surat"/></td>
                <td><input type="submit" value="Search" /></td>
            </tr>
        </table>

    </div>
    <div className='tableemail-container'>
        <div className='tabledashboard'>
        <table border="1" cellspacing="0" width="1000px" height="60px">
        <thead>
                    <th>No</th>
                    <th>PIC</th>
                    <th>Penunjukan</th>
                    <th>Nomor Sperj</th>
                    <th>Tgl Perjanjian</th>
                    <th>Nama Vendor</th>
                    <th>Perwakilan</th>
                    <th >Nama Perjanjian</th>
                    <th>Obyek Perjanjian</th>
                    <th>Nilai Perjanjian</th>
                    <th>Pajak</th>
                    <th>Tanggal Mulai</th>
                    <th>Tanggal Selesai</th>
                    <th>File</th>
                    <th>Keterangan</th>
                    <th>Action</th>
                    <th>ADD</th>
               
                </thead>
                  <tbody>
                  {this.usersList()}
                </tbody> 
        </table>
        </div>
        
    </div>
    </div>
    
      )
    }

  
}

export default DSperj