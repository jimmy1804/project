import React, {Component} from 'react';
import axios from 'axios';
import '../App.css'
import RecordSpkAdd from './RecordSpkAdd';

class DSpkAdd extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            dataspk: []
        })
    }

    componentDidMount() {
        var url = window.$apiBaseURL + '/get-dataspk-add';
        var token = localStorage.getItem('token');
        var config = {
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json',
                Authorization: `Bearer `+token
            },
        };
        axios.get(url,config).then(response => {
            this.setState({dataspk: response.data.results});
        }).catch(function (error) {
            window.location.href="/";
                console.log(error);
            })
    }

    usersList() {
        return this.state.dataspk.map(function (object, index) {
            return <RecordSpkAdd formData={object} key={index}/>;
        });
    }


    render() {
        return (
            <div>
                <div className='DEmail'>Data SPK ADD</div>

                <div className='Search1'>
                    <table>
                        <tr>
                            <td><input type="text" id="searchnomor" placeholder="Cari Surat"/></td>
                            <td><input type="submit" value="Search"/></td>
                        </tr>
                    </table>

                </div>
                <div className='tableemail-container'>
                    <div className='tabledashboard'>
                        <table border="1" cellspacing="0" width="1000px" height="60px">
                            <thead>
                            <th>No</th>
                            <th>PIC</th>
                            <th>SPK Awal</th>
                            <th>No. ADD SPK</th>
                            <th>Tgl ADD SPK</th>
                            <th>Nama Vendor / Kontraktor</th>
                            <th>Perwakilan</th>
                            <th >Perihal</th>
                            <th>Perubahan/tambahan SPK</th>
                            <th>Perubahan/tambahan SPK</th>
                            <th>Tanggal Mulai</th>
                            <th>Tanggal Selesai</th>
                            <th>File</th>
                            <th>Keterangan</th>
                            <th>Action</th>

                            </thead>
                            <tbody>
                            {this.usersList()}
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

        )
    }


}

export default DSpkAdd