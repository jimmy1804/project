import React, {Component} from 'react';
import axios from 'axios';
import Form from 'react-bootstrap/Form';
import {
    BrowserRouter as Router,
    Link,
    Route,
    Routes,
    useNavigate,
    useParams,
} from "react-router-dom";

class EditPenerimaEmail extends Component {
    

    constructor(props) {
        

        super(props);
        this.onChangegender = this.onChangegender.bind(this);
        this.onChangewanita = this.onChangewanita.bind(this);
        this.state = ({
            gender:'',
     
            penerima_email: [],

        })

        this.handleClick = this.handleClick.bind(this);
    }
    onChangegender(e) {
        this.setState({
            gender: "Pria",
           
            
            
            
        });
    }
    onChangewanita(e) {
        this.setState({
            gender: "Wanita",
           
            
            
            
        });
    }

   


    componentDidMount() {
      
        var token = localStorage.getItem('token');
        var config = {
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json',
                Authorization: `Bearer ` + token
            },
        };
        var url = window.$apiBaseURL + '/get-penerimaemail/' + this.props.params.id;
        axios.get(url, config).then(response => {
           console.log(response.data);
            this.setState({penerima_email: response.data.results});
        })
            .catch(function (error) {
                console.log(error);
            })
    }

    handleClick() {

        var penerima_email = this.state.penerima_email;
        const formData = new FormData();

        formData.append('nama', penerima_email.nama);
        formData.append('email_penerima', penerima_email.email_penerima);
        formData.append('gender', this.state.gender);
        formData.append('genderr', this.state.gender);

           // formData.append('user_username', localStorage.getItem('user_username'));

        var token = localStorage.getItem('token');
        var config = {
            headers: {Authorization: `Bearer `+token}
        };
        const data_email = this.props.params.id
        var url = window.$apiBaseURL + `/post-dataemail/${data_email}`;

        axios.post(url, formData, config).then((response) => {
            alert("berhasil");
            if (response.data.status) {
            console.log(response.data);
               // window.location.href="/Jemail";
            }
        }).catch((error) => {
            window.location.href="/";
            console.log(error.response.data);
        });

/*
        var token = localStorage.getItem('token');
        var config = {
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json',
                Authorization: `Bearer ` + token
            },
        };
        var url = window.$apiBaseURL + '/post-composespk-adamdum';


        axios.post(url, config,formData).then((response) => {
            // console.log(response);
            // alert("berhasil");
            if (response.data.status) {


            }
        }).catch((error) => {
            console.log(error.response.data);
          
            // console.log(error, "ini error");
        });
*/
    }

    render() {
        var _edit = localStorage.getItem('_edit');
        if(_edit == 0) {
            window.location.href='/DSpk';
            return;
        } else {

            var penerima_email = this.state.penerima_email;

            return (
                <div>
                    <div className='SPKADD'>Edit Penerima Email</div>
                    <div className='FormSPKADD'>
                        <table>
                        <tr>
                            <td>Nama Penerima</td>
                            <td><Form.Control type="text" id="nama" value={penerima_email.nama}
                                              onChange={this.handleChange}/></td>
                        </tr>
                        <tr>
                            <td>Penerima Email</td>
                            <td><Form.Control type="email" id="email_penerima" value={penerima_email.email_penerima}
                                             onChange={this.handleChange}/></td>
                        </tr>
                        <tr>
                            <td>Gender</td>
                            <td><Form.Check type="radio" id="gender" value={this.state.gender=="Pria"}
                                            onClick={() => this.onChangegender('Pria')}/>Pria</td>
                            <td><Form.Check type="radio" id="gender" value={this.state.gender=="Wanita"}
                                            onClick={() => this.onChangewanita('Wanita')}/>Wanita</td>

                        </tr>
                        <tr>
                                <td><input type="submit" value="Submit" onClick={this.handleClick}/></td>
                            </tr>

                        </table>

                    </div>
                </div>
            )
        }
    }

    handleChange = (event) => {
        const {id, value} = event.target;
        this.setState((prevState) => ({
            penerima_email: {
                ...prevState.penerima_email,
                [id]: value
            }
        }));
    };

}

export default (props) => {
    const params = useParams();

  
  

    return (
        <EditPenerimaEmail {...props} params={params}/>
    );
}
