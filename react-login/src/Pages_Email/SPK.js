import React, {Component, useEffect, useState} from 'react';
import axios from 'axios';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import ReactTags from 'react-tag-autocomplete';

function SPK() {
    const [pic, setPic] = useState("");
    const [suratpenunjukan, setSuratPenunjukan] = useState("");
    const [nomorspk, setNomorSpk] = useState("");
    const [tanggalspk, setTanggalSpk] = useState("");
    const [namavendor, setNamaVendor] = useState("");
    const [perwakilan, setPerwakilan] = useState("");
    const [pekerjaan, setPekerjaan] = useState("");
    const [nilaispk, setNilaiSpk] = useState("");
    const [pajak, setPajak] = useState("");
    const [tanggalmulai, setTanggalMulai] = useState("");
    const [tanggalselesai, setTanggalSelesai] = useState("");
    const [keterangan, setKeterangan] = useState("");
    const [file, setFile] = useState("");

    const [tags, setTags] = useState([]);
    const [suggestions, setSuggestions] = useState([]);
    const [emailtemplate, setemailtemplate] = useState([]);
    const [opendiv, setOpenDiv] = useState(false);
    const [validation, setValidation] = useState([]);
    const [judulemail, setJudulEmail]   = useState("");
    const [isiemail, setIsiEmail]   = useState("");

    useEffect(() => {
        async function fetch() {
            var urlGetEmailAndTemplate = window.$apiBaseURL + '/get-dataemail-and-template';

            var token = localStorage.getItem('token');
            var config = {
                headers: {Authorization: `Bearer `+token}
            };

            await axios.get(urlGetEmailAndTemplate, config).then(response => {
                if (response.data.status) {
                    setSuggestions(response.data.results.email);
                    setemailtemplate(response.data.results.template);
                } else {
                    setemailtemplate([]);
                    setSuggestions([]);
                }
            }).catch(function (error) {
                window.location.href    = '/';
                console.log(error);
                return [];
            });
        }

        fetch();
    }, []);


    function onDelete(i) {
        const tempTags = tags.slice(0);
        tempTags.splice(i, 1);
        setTags(tempTags);
    }

    function onAddition(tag) {
        tags.concat(tag);
        setTags(tags.concat(tag));
    }


    const SubmitHandler = async (e) => {
        e.preventDefault();
        var penerimaemailstring = tags.map(function(elem){
            return elem.id;
        }).join(";");

        const formData = new FormData();
        formData.append('pic', pic);
        formData.append('suratpenunjukan', suratpenunjukan);
        formData.append('nomorspk', nomorspk);
        formData.append('tanggalspk', tanggalspk);
        formData.append('namavendor', namavendor);
        formData.append('perwakilan', perwakilan);
        formData.append('pekerjaan', pekerjaan);
        formData.append('nilaispk', nilaispk);
        formData.append('pajak', pajak);
        formData.append('tanggalmulai', tanggalmulai);
        formData.append('tanggalselesai', tanggalselesai);
        formData.append('keterangan', keterangan);
        formData.append('user_id', localStorage.getItem('user_id'));
        formData.append('file', file);
        formData.append('user_username', localStorage.getItem('user_username'));
        formData.append('judul_email', judulemail);
        formData.append('isi_email', isiemail);
        formData.append('penerima_email', penerimaemailstring);

        var token = localStorage.getItem('token');
        var config = {
            headers: {Authorization: `Bearer `+token}
        };
        var url = window.$apiBaseURL + '/post-composespk';

        await axios.post(url, formData, config).then((response) => {
            alert("berhasil");
            if (response.data.status) {
                window.location.href="DSpk";
            }
        }).catch((error) => {
            //window.location.href="/";
            console.log(error.response.data);
        });
    };

    function onChangeEmailTemplate(e) {
        var id      = e.target.value;
        var found   = emailtemplate.find(x => x.id == id);
        if(found) {
            setIsiEmail(found.isi_email);
            setJudulEmail(found.title);
        }
    }

    return (
        <div>
            <div className='SPK'>SPK</div>
            <div className='FormSPK'>
                <Form>
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                PIC
                                <br />
                                <code>&#123;pic&#125;</code>
                            </td>
                            <td>
                                <Form.Group className="mb-3">
                                    <Form.Control type="text" id="pic" value={pic}
                                                  onChange={(e) => setPic(e.target.value)}/>
                                </Form.Group>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Surat Penunjukan
                                <br />
                                <code>&#123;suratpenunjukan&#125;</code>
                            </td>
                            <td>
                                <Form.Group className="mb-3">
                                    <Form.Control type="text" id="suratpenunjukna" value={suratpenunjukan}
                                                  onChange={(e) => setSuratPenunjukan(e.target.value)}/>
                                </Form.Group>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Nomor SPK
                                <br />
                                <code>&#123;nomorspk&#125;</code>
                            </td>
                            <td>
                                <Form.Group className="mb-3">
                                    <Form.Control type="text" id="nomorspk" value={nomorspk}
                                                  onChange={(e) => setNomorSpk(e.target.value)}/>
                                </Form.Group>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Tanggal SPK
                                <br />
                                <code>&#123;tanggalspk&#125;</code>
                            </td>
                            <td>
                                <Form.Group className="mb-3">
                                    <Form.Control type="date" id="tanggalspk" value={tanggalspk}
                                                  onChange={(e) => setTanggalSpk(e.target.value)}/>
                                </Form.Group>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Nama Vendor / Kontraktor
                                <br />
                                <code>&#123;namavendor&#125;</code>
                            </td>
                            <td>
                                <Form.Control type="text" id="namavendor" value={namavendor}
                                              onChange={(e) => setNamaVendor(e.target.value)}/></td>
                        </tr>
                        <tr>
                            <td>
                                Perwakilan (dasar kewewenangan)
                                <br />
                                <code>&#123;perwakilan&#125;</code>
                            </td>
                            <td><Form.Control type="text" id="perwakilan" value={perwakilan}
                                              onChange={(e) => setPerwakilan(e.target.value)}/></td>
                        </tr>
                        <tr>
                            <td>
                                Pekerjaan
                                <br />
                                <code>&#123;pekerjaan&#125;</code>
                            </td>
                            <td><Form.Control type="text" id="pekerjaan" value={pekerjaan}
                                              onChange={(e) => setPekerjaan(e.target.value)}/></td>
                        </tr>
                        <tr>
                            <td>
                                Nilai SPK
                                <br />
                                <code>&#123;nilaispk&#125;</code>
                            </td>
                            <td><Form.Control type="text" id="nilaispk" value={nilaispk}
                                              onChange={(e) => setNilaiSpk(e.target.value)}/></td>
                        </tr>
                        <tr>
                            <td>
                                Pajak
                                <br />
                                <code>&#123;pajak&#125;</code>
                            </td>
                            <td><Form.Control type="text" id="pajak" value={pajak}
                                              onChange={(e) => setPajak(e.target.value)}/></td>
                        </tr>
                        <tr>
                            <td>
                                Tanggal Mulai
                                <br />
                                <code>&#123;tanggalmulai&#125;</code>
                            </td>
                            <td><Form.Control type="date" id="tanggalmulai" value={tanggalmulai}
                                              onChange={(e) => setTanggalMulai(e.target.value)}/></td>
                        </tr>
                        <tr>
                            <td>
                                Tanggal Selesai
                                <br />
                                <code>&#123;tanggalselesai&#125;</code>
                            </td>
                            <td><Form.Control type="date" id="tanggalselesai" value={tanggalselesai}
                                              onChange={(e) => setTanggalSelesai(e.target.value)}/></td>
                        </tr>
                        <tr>
                            <td>
                                Keterangan
                                <br />
                                <code>&#123;keterangan&#125;</code>
                            </td>
                            <td><Form.Control as="textarea" rows="5" cols="50" id="keterangan" value={keterangan}
                                          onChange={(e) => setKeterangan(e.target.value)}/></td>
                        </tr>
                        <tr>
                            <td>
                                <p>Lampiran</p>
                            </td>
                            <td>
                                <Form.Control type="file" name="upload" id='file' multiple
                                              onChange={(e) => setFile(e.target.files[0])}/></td>
                        </tr>
                        <tr>
                            <td>Tujuan Email</td>
                            <td>
                                <ReactTags
                                    tags={tags}
                                    suggestions={suggestions}
                                    onDelete={onDelete.bind(this)}
                                    placeholderText="Silahkan masukkan email"
                                    onAddition={onAddition.bind(this)}/>
                            </td>
                        </tr>
                        <tr>
                            <td>Template Email</td>
                            <td>
                                <Form.Select onChange={onChangeEmailTemplate.bind(this)}>
                                    <option>Pilih template email</option>
                                    {emailtemplate.map((item, index) => (
                                        <option key={index} value={item.id}>
                                            {item.judul_template}
                                        </option>
                                    ))}
                                </Form.Select>
                            </td>
                        </tr>
                        <tr>
                            <td>Judul Email</td>
                            <td><Form.Control type="text" id="judulemail" value={judulemail} onChange={(e) => setJudulEmail(e.target.value)} /></td>
                        </tr>
                        <tr>
                            <td width="300">
                                Isi Email
                                <br />
                                <code>
                                    contoh:
                                    gunakan &#123;nama_penerima&#125; untuk menghasilkan nama penerima
                                </code>
                            </td>
                            <td>
                                <textarea rows="5" cols="50" id="isiemail" value={isiemail} onChange={(e) => setIsiEmail(e.target.value)}></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Button onClick={SubmitHandler}>Submit</Button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </Form>

            </div>
        </div>
    )
}

export default SPK