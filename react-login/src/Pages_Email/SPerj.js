import React, {Component, useEffect, useState} from 'react';
import axios from 'axios';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import ReactTags from 'react-tag-autocomplete';

function SPerj() {
    const [pic, setPic] = useState("");
    const [penunjukan, setPenunjukan] = useState("");
    const [nomorsperj, setNomorSperj] = useState("");
    const [tglperjanjian, setTglPerjanjian] = useState("");
    const [namavendor, setNamaVendor] = useState("");
    const [perwakilan, setPerwakilan] = useState("");
    const [namaperjanjian, setNamaPerjanjian] = useState("");
    const [obyekperjanjian, setObyekPerjanjian] = useState("");
    const [nilaiperjanjian, setNilaiPerjanjian] = useState("");
    const [pajak, setPajak] = useState("");
    const [tanggalmulai, setTanggalMulai] = useState("");
    const [tanggalselesai, setTanggalSelesai] = useState("");
    const [file, setFile] = useState("");
    const [keterangan, setKeterangan] = useState("");


    const [validation, setValidation] = useState([]);

    const SubmitHandler = async (e) => {
        e.preventDefault();

        const formData = new FormData();

        formData.append('pic', pic);
        formData.append('penunjukan', penunjukan);
        formData.append('nomorsperj', nomorsperj);
        formData.append('tglperjanjian', tglperjanjian);
        formData.append('namavendor', namavendor);
        formData.append('perwakilan', perwakilan);
        formData.append('namaperjanjian', namaperjanjian);
        formData.append('obyekperjanjian', obyekperjanjian);
        formData.append('nilaiperjanjian', nilaiperjanjian);
        formData.append('pajak', pajak);
        formData.append('tanggalmulai', tanggalmulai);
        formData.append('tanggalselesai', tanggalselesai);
        formData.append('file', file);
        formData.append('keterangan', keterangan);
        formData.append('user_id', localStorage.getItem('user_id'));

        formData.append('user_username', localStorage.getItem('user_username'));

        var token = localStorage.getItem('token');
        var config = {
            headers: {Authorization: `Bearer `+token}
        };
        var url = window.$apiBaseURL + '/post-composesperj';

        await axios.post(url, formData, config).then((response) => {
            alert("berhasil");
            if (response.data.status) {
                window.location.href="DSperj";
            }
        }).catch((error) => {
            //window.location.href="";
            console.log(error.response.data);
        });

        /*
        var token = localStorage.getItem('token');
        var config = {
            headers: {Authorization: `Bearer `+token}
        };
        var url = window.$apiBaseURL + 'post-composesperj';
        await axios.post(url, formData, config).then((response) => {
            alert("berhasil");
            if (response.data.status) {
                window.location.href="DSperj";
            }
        }).catch((error) => {
            window.location.href="/";
            console.log(error.response.data);
        });

*/
    };
    

    return (
        <div>
            <div className='SPK'>SPerj</div>
            <div className='FormSPK'>
                <table>
                <tr>
                            <td>
                                PIC
                                <br />
                              
                            </td>
                            <td>
                                <Form.Group className="mb-3">
                                    <Form.Control type="text" id="pic" value={pic}
                                                  onChange={(e) => setPic(e.target.value)}/>
                                </Form.Group>
                            </td>
                        </tr>
                    <tr>
                        <td>Penunjukan/Persetujuan</td>
                        <td><Form.Control type="text" id="penunjukan" value={penunjukan}
                                          onChange={(e) => setPenunjukan(e.target.value)}/></td>
                    </tr>
                    <tr>
                        <td>Nomor SPerj</td>
                        <td><Form.Control type="text" id="nomorsperj" value={nomorsperj}
                                          onChange={(e) => setNomorSperj(e.target.value)}/></td>
                    </tr>
                    <tr>
                        <td>Tgl Perjanjian</td>
                        <td><Form.Control type="date" id="tglperjanjian" value={tglperjanjian}
                                          onChange={(e) => setTglPerjanjian(e.target.value)}/></td>
                    </tr>
                    <tr>
                        <td>Nama Vendor / Pihak Kerja Sama</td>
                        <td><Form.Control type="text" id="namavendor" value={namavendor}
                                          onChange={(e) => setNamaVendor(e.target.value)}/></td>
                    </tr>
                    <tr>
                        <td>Perwakilan</td>
                        <td><Form.Control type="text" id="perwakilan" value={perwakilan}
                                          onChange={(e) => setPerwakilan(e.target.value)}/></td>
                    </tr>
                    <tr>
                        <td>Nama Perjanjian</td>
                        <td><Form.Control type="text" id="namaperjanjian" value={namaperjanjian}
                                          onChange={(e) => setNamaPerjanjian(e.target.value)}/></td>
                    </tr>
                    <tr>
                        <td>Obyek Perjanjian</td>
                        <td><Form.Control type="text" id="obyekperjanjian" value={obyekperjanjian}
                                          onChange={(e) => setObyekPerjanjian(e.target.value)}/></td>
                    </tr>
                    <tr>
                        <td>Nilai Perjanjian</td>
                        <td><Form.Control type="text" id="nilaiperjanjian" value={nilaiperjanjian}
                                          onChange={(e) => setNilaiPerjanjian(e.target.value)}/></td>
                    </tr>
                    <tr>
                        <td>Pajak</td>
                        <td><Form.Control type="text" id="pajak" value={pajak}
                                          onChange={(e) => setPajak(e.target.value)}/></td>
                    </tr>
                    <tr>
                        <td>Tanggal Mulai</td>
                        <td><Form.Control type="date" id="tanggalmulai" value={tanggalmulai}
                                          onChange={(e) => setTanggalMulai(e.target.value)}/></td>
                    </tr>
                    <tr>
                        <td>Tanggal Selesai</td>
                        <td><Form.Control type="date" id="tanggalselesai" value={tanggalselesai}
                                          onChange={(e) => setTanggalSelesai(e.target.value)}/></td>
                    </tr>
                    <tr>
                        <td>Keterangan</td>
                        <td><textarea id="keterangan" value={keterangan} rows="5" cols="50"
                                      onChange={(e) => setKeterangan(e.target.value)}></textarea></td>
                    </tr>
                    <tr>
                        <td>Lampiran</td>
                        <td><Form.Control type="file" name="upload" id='file'
                                          onChange={(e) => setFile(e.target.files[0])}/></td>
                    </tr>
                    <tr>
                        <td>
                            <Button onClick={SubmitHandler}>submit</Button>
                        </td>
                    </tr>

                </table>

            </div>
        </div>
    )
}

export default SPerj