import React, {Component, useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';
import RecordTemplateEmail from './RecordTemplateEmail';
import Table from 'react-bootstrap/Table';


class JEmailTemplate extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            template_email: []
        })
    }

    componentDidMount() {
        var token = localStorage.getItem('token');
        var config = {
            headers: {Authorization: `Bearer `+token}
        };
        var url = window.$apiBaseURL + '/get-templateemail';
        axios.get(url, config).then(response => {
            this.setState({template_email: response.data.results});
        })
            .catch(function (error) {
                console.log(error);
            })
    }

    usersList() {
        // console.log("asd");
        // console.log(this.state.dsurat);

        return this.state.template_email.map(function (object, index) {
            return <RecordTemplateEmail formData={object} key={index}/>;
        });
        // console.log(data);


    }


    render() {
        return (
            <div>
                <div className='JEmail'>Jenis Email</div>
                <div className='Search2'>
                    <table>
                        <tr>
                            <td><Link to='/JenisEmailTambahTemplate'><input type="button" value="Tambah"
                                                                            className='Tambah1'/></Link></td>
                        </tr>
                    </table>

                </div>
                <div className='tableemail-container'>
                    <div className='tabledashboard'>
                        <table border="1" cellspacing="0" width="1000px" height="60px">
                            <thead>
                            <th>No</th>
                            <th>Judul Template</th>
                            <th>Title</th>
                            <th>IsiEmail</th>
                            <th>Action</th>


                            </thead>
                            <tbody>
                            {this.usersList()}
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

        )
    }


}

export default JEmailTemplate