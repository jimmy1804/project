import React, {Component, useEffect, useState} from 'react';
import axios from 'axios';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

function JenisEmailTambahTemplate() {
    const [title, setTitle] = useState("");
    const [isiemail, setIsiEmail] = useState("");
    const [judultemplate, setJudulTemplate] = useState("");


    const SubmitHandler = async (e) => {
        e.preventDefault();

        const formData = new FormData();
        formData.append('title', title);
        formData.append('isiemail', isiemail);
        formData.append('judultemplate', judultemplate);

        var token = localStorage.getItem('token');
        var config = {
            headers: {Authorization: `Bearer `+token}
        };
        var url = window.$apiBaseURL + '/post-composetemplate';
        await axios.post(url, formData,config).then((response) => {
            alert("berhasil");

            if (response.data.status) {
                window.location.href="/JEmailTemplate";
            }
        }).catch((error) => {
            console.log(error.response.data);
            window.location.href="/";
        });


    };


    return (
        <div>
            <div className='JEmailt'>Buat Email</div>
            <div className='Jenis-Email'>
                <table>
                    <tr>
                        <td>Judul Template</td>
                        <td><Form.Control type="text" id="judultemplate" value={judultemplate}
                                   onChange={(e) => setJudulTemplate(e.target.value)}/></td>
                    </tr>
                    <tr>
                        <td>Title</td>
                        <td><Form.Control type="text" id="title" value={title}
                                   onChange={(e) => setTitle(e.target.value)}/></td>
                    </tr>
                    <tr>
                        <td>Isi Email</td>
                        <td><textarea type="text" id="isiemail" value={isiemail}
                                      onChange={(e) => setIsiEmail(e.target.value)} rows="10" cols="50"></textarea></td>
                    </tr>


                    <tr>
                        <td>
                            <Button onClick={SubmitHandler}>submit</Button>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    )
}

export default JenisEmailTambahTemplate