import React, {Component} from 'react';
import Form from 'react-bootstrap/Form';
import axios from 'axios';
import {
    BrowserRouter as Router,
    Link,
    Route,
    Routes,
    useParams,
} from "react-router-dom";

class EditSperj extends Component {

    constructor(props) {

        super(props);
        
        this.onChangefile = this.onChangefile.bind(this);
        this.state = ({

            datasperj: []

        })

        this.handleClick = this.handleClick.bind(this);
    }


    onChangefile(e) {
        let files = e.target.files;
        // console.log(files[0]);
        // this.setState({ file: files[0] }, () => { console.log(this.state.files) });
        this.setState({
            file: files[0]
        });
    }


    componentDidMount() {
        var token = localStorage.getItem('token');
        var config = {
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json',
                Authorization: `Bearer ` + token
            },
        };
        var url = window.$apiBaseURL + '/get-datasperj/' + this.props.params.id;
        axios.get(url, config).then(response => {
            console.log(response.data);
            this.setState({datasperj: response.data.results});
        })
            .catch(function (error) {
                console.log(error);
            })
    }

    handleClick() {

        var datasperj = this.state.datasperj;
        const formData = new FormData();

        formData.append('pic', datasperj.pic);
        formData.append('penunjukan', datasperj.penunjukan);
        formData.append('nomorsperj', datasperj.nomorsperj);
        formData.append('tglperjanjian', datasperj.tglperjanjian);
        formData.append('namavendor', datasperj.namavendor);
        formData.append('perwakilan', datasperj.perwakilan);
        formData.append('namaperjanjian', datasperj.namaperjanjian);
        formData.append('obyekperjanjian', datasperj.obyekperjanjian);
        formData.append('nilaiperjanjian', datasperj.nilaiperjanjian);
        formData.append('pajak', datasperj.pajak);
        formData.append('tanggalmulai', datasperj.tanggalmulai);
        formData.append('tanggalselesai', datasperj.tanggalselesai);
        formData.append('keterangan', datasperj.keterangan);
        formData.append('file', this.state.file);


        var token = localStorage.getItem('token');
        var config = {
            headers: {Authorization: `Bearer `+token}
        };
        const data_sperj = this.props.params.id
        var url = window.$apiBaseURL + `/post-datasperj/${data_sperj}`;

        axios.post(url, formData, config).then((response) => {
            alert("berhasil");
            if (response.data.status) {
                console.log(response.data);
               // window.location.href="/DSpk";
            }
        }).catch((error) => {
           // window.location.href="/";
            console.log(error.response.data);
        });

        /*
        var url = window.$apiBaseURL + '/post-composesperj-add';
        axios.post(url, formData).then((response) => {
            // console.log(response);
            // alert("berhasil");
            if (response.data.status) {


            }
        }).catch((error) => {
            console.log(error.response.data);
            // console.log(error, "ini error");
        });
        */
    }

    render() {
        

        var datasperj = this.state.datasperj;

        return (
            
            <div>
                <div className='SPKADD'>Edit SPerj</div>
                <div className='FormSPKADD'>
                    <table>
                    <tr>
                            <td>
                                PIC
                                <br />
                              
                            </td>
                            <td>
                                <Form.Group className="mb-3">
                                    <Form.Control type="text" id="pic" value={datasperj.pic}
                                                  onChange={this.handleChange}/>
                                </Form.Group>
                            </td>
                        </tr>
                    <tr>
                        <td>Penunjukan/Persetujuan</td>
                        <td><Form.Control type="text" id="penunjukan" value={datasperj.penunjukan}
                                         onChange={this.handleChange}/></td>
                    </tr>
                    <tr>
                        <td>Nomor SPerj</td>
                        <td><Form.Control type="text" id="nomorsperj" value={datasperj.nomorsperj}
                                          onChange={this.handleChange}/></td>
                    </tr>
                    <tr>
                        <td>Tgl Perjanjian</td>
                        <td><Form.Control type="date" id="tglperjanjian" value={datasperj.tglperjanjian}
                                          onChange={this.handleChange}/></td>
                    </tr>
                    <tr>
                        <td>Nama Vendor / Pihak Kerja Sama</td>
                        <td><Form.Control type="text" id="namavendor" value={datasperj.namavendor}
                                          onChange={this.handleChange}/></td>
                    </tr>
                    <tr>
                        <td>Perwakilan</td>
                        <td><Form.Control type="text" id="perwakilan" value={datasperj.perwakilan}
                                         onChange={this.handleChange}/></td>
                    </tr>
                    <tr>
                        <td>Nama Perjanjian</td>
                        <td><Form.Control type="text" id="namaperjanjian" value={datasperj.namaperjanjian}
                                         onChange={this.handleChange}/></td>
                    </tr>
                    <tr>
                        <td>Obyek Perjanjian</td>
                        <td><Form.Control type="text" id="obyekperjanjian" value={datasperj.obyekperjanjian}
                                          onChange={this.handleChange}/></td>
                    </tr>
                    <tr>
                        <td>Nilai Perjanjian</td>
                        <td><Form.Control type="text" id="nilaiperjanjian" value={datasperj.nilaiperjanjian}
                                          onChange={this.handleChange}/></td>
                    </tr>
                    <tr>
                        <td>Pajak</td>
                        <td><Form.Control type="text" id="pajak" value={datasperj.pajak}
                                        onChange={this.handleChange}/></td>
                    </tr>
                    <tr>
                        <td>Tanggal Mulai</td>
                        <td><Form.Control type="date" id="tanggalmulai" value={datasperj.tanggalmulai}
                                         onChange={this.handleChange}/></td>
                    </tr>
                    <tr>
                        <td>Tanggal Selesai</td>
                        <td><Form.Control type="date" id="tanggalselesai" value={datasperj.tanggalselesai}
                                         onChange={this.handleChange}/></td>
                    </tr>
                    <tr>
                        <td>Keterangan</td>
                        <td><textarea id="keterangan" value={datasperj.keterangan}
                                              onChange={this.handleChange}></textarea></td>
                    </tr>
                    <tr>
                        <td>Lampiran</td>
                        <td><Form.Control type="file" name="upload" id='file'
                                          onChange={this.onChangefile}/></td>
                    </tr>
                    <tr>
                    <td><input type="submit" value="Submit" onClick={this.handleClick}/></td>
                    </tr>
                    </table>

                </div>
            </div>
        )

    }

    handleChange = (event) => {
        const {id, value} = event.target;
        this.setState((prevState) => ({
            datasperj: {
                ...prevState.datasperj,
                [id]: value
            }
        }));


    };

}

export default (props) => {
    const params = useParams();

    return (
        <EditSperj {...props} params={params}/>
    );
}
