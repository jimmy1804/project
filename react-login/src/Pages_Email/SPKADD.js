import React, {Component} from 'react';
import axios from 'axios';
import Form from 'react-bootstrap/Form';

import {
    BrowserRouter as Router,
    Link,
    Route,
    Routes,
    useNavigate,
    useParams,
} from "react-router-dom";

class SPKADD extends Component {
    

    constructor(props) {

        super(props);
        this.onChangenoaddspk = this.onChangenoaddspk.bind(this);
        this.onChangetgladdspk = this.onChangetgladdspk.bind(this);
        this.onChangeperihal = this.onChangeperihal.bind(this);
        this.onChangeperubahan = this.onChangeperubahan.bind(this);
        this.onChangeperubahan2 = this.onChangeperubahan2.bind(this);
        this.onChangetglmulai = this.onChangetglmulai.bind(this);
        this.onChangetglselesai = this.onChangetglselesai.bind(this);
        this.onChangeketerangan = this.onChangeketerangan.bind(this);
        this.onChangefile = this.onChangefile.bind(this);
        this.state = ({
            noaddspk: '',
            tgladdspk: '',
            perihal: '',
            perubahan: '',
            perubahan2: '',
            tglmulai: '',
            tglselesai: '',
            keterangan: '',
            file: '',
            dataspk: []

        })

        this.handleClick = this.handleClick.bind(this);
    }

    onChangenoaddspk(e) {
        this.setState({
            noaddspk: e.target.value
        });
    }

    onChangetgladdspk(e) {
        this.setState({
            tgladdspk: e.target.value
        });
    }

    onChangeperihal(e) {
        this.setState({
            perihal: e.target.value
        });
    }

    onChangeperubahan(e) {
        this.setState({
            perubahan: e.target.value
        });
    }

    onChangeperubahan2(e) {
        this.setState({
            perubahan2: e.target.value
        });
    }

    onChangetglmulai(e) {
        this.setState({
            tglmulai: e.target.value
        });
    }

    onChangetglselesai(e) {
        this.setState({
            tglselesai: e.target.value
        });
    }

    onChangeketerangan(e) {
        this.setState({
            tglmulai: e.target.value
        });
    }

    onChangefile(e) {
        let files = e.target.files;
        // console.log(files[0]);
        // this.setState({ file: files[0] }, () => { console.log(this.state.files) });
        this.setState({
            file: files[0]
        });
    }


    componentDidMount() {
        var token = localStorage.getItem('token');
        var config = {
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json',
                Authorization: `Bearer ` + token
            },
        };
        var url = window.$apiBaseURL + '/get-dataspk/' + this.props.params.id;
        axios.get(url, config).then(response => {
            this.setState({dataspk: response.data.results});
        })
            .catch(function (error) {
                console.log(error);
            })
    }

    handleClick() {

        var dataspk = this.state.dataspk;
        const formData = new FormData();

        formData.append('surat_awal_id', this.props.params.id);
        formData.append('nomorspk', dataspk.nomorspk);
        formData.append('pic', dataspk.pic);
        formData.append('noaddspk', this.state.noaddspk);
        formData.append('tgladdspk', this.state.tgladdspk);
        formData.append('namavendor', dataspk.namavendor);
        formData.append('perwakilan', dataspk.perwakilan);
        formData.append('perihal', this.state.perihal);
        formData.append('perubahan', this.state.perubahan);
        formData.append('perubahan2', this.state.perubahan2);
        formData.append('tglmulai', this.state.tglmulai);
        formData.append('tglselesai', this.state.tglselesai);
        formData.append('file', this.state.file);


        var token = localStorage.getItem('token');
        var config = {
            headers: {Authorization: `Bearer `+token}
        };
        var url = window.$apiBaseURL + '/post-composespk-adamdum';

        axios.post(url, formData, config).then((response) => {
            alert("berhasil");
            if (response.data.status) {
                console.log(response.data);
                //window.location.href="DSpkAdd";
            }
        }).catch((error) => {
            window.location.href="/";
            console.log(error.response.data);
        });

/*
        var token = localStorage.getItem('token');
        var config = {
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json',
                Authorization: `Bearer ` + token
            },
        };
        var url = window.$apiBaseURL + '/post-composespk-adamdum';


        axios.post(url, config,formData).then((response) => {
            // console.log(response);
            // alert("berhasil");
            if (response.data.status) {


            }
        }).catch((error) => {
            console.log(error.response.data);
          
            // console.log(error, "ini error");
        });
*/
    }

    render() {
        var _edit = localStorage.getItem('_edit');
        if(_edit == 0) {
            window.location.href='/DSpk';
            return;
        } else {

            var dataspk = this.state.dataspk;

            return (
                <div>
                    <div className='SPKADD'>SPK ADD</div>
                    <div className='FormSPKADD'>
                        <Form>
                            <table>
                        <tr>
                                <td>PIC</td>

                                <td><input type="text" id="pic" value={dataspk.pic}
                                           onChange={this.handleChange}/></td>
                            </tr>
                            <tr>
                            
                                <td>SPK Awal</td>
                                <td id="nomorspk">{dataspk.nomorspk}</td>
                            </tr>
                            <tr>
                                <td>No.ADD SPK</td>
                                <td><input type="text" id="noaddspk" value={this.state.noaddspk}
                                           onChange={this.onChangenoaddspk}/></td>
                            </tr>
                            <tr>
                                <td>Tgl ADD SPK</td>
                                <td><input type="text" id="tgaddspk" value={this.state.tgladdspk}
                                           onChange={this.onChangetgladdspk}/></td>
                            </tr>
                            <tr>
                                <td>Nama Vendor / Kontraktor</td>

                                <td><input type="text" id="namavendor" value={dataspk.namavendor}
                                           onChange={this.handleChange}/></td>
                            </tr>
                            <tr>
                                <td>Perwakilan (dasar kewewenangan)</td>
                                <td><input type="text" id="perwakilan" value={dataspk.perwakilan}
                                           onChange={this.handleChange}/></td>
                            </tr>
                            <tr>
                                <td>Perihal</td>
                                <td><input type="text" id="perihal" value={this.state.perihal}
                                           onChange={this.onChangeperihal}/></td>
                            </tr>
                            <tr>
                                <td>Perubahan/Tambahan SPK</td>
                                <td><input type="text" id="perubahan" value={this.state.perubahan}
                                           onChange={this.onChangeperubahan}/></td>
                            </tr>
                            <tr>
                                <td>Perubahan/Tambahan SPK</td>
                                <td><input type="text" id="perubahan2" value={this.state.perubahan2}
                                           onChange={this.onChangeperubahan2}/></td>
                            </tr>
                            <tr>
                                <td>Tanggal Mulai</td>
                                <td><input type="text" id="tglmulai" value={this.state.tglmulai}
                                           onChange={this.onChangetglmulai}/></td>
                            </tr>
                            <tr>
                                <td>Tanggal Selesai</td>
                                <td><input type="text" id="tglselesai" value={this.state.tglselesai}
                                           onChange={this.onChangetglselesai}/></td>
                            </tr>
                            <tr>
                                <td>Keterangan</td>
                                <td><textarea id="keterangan" value={this.state.keterangan}
                                              onChange={this.onChangeketerangan}></textarea></td>
                            </tr>
                            <div className='lampiran1'>
                                <p>Lampiran</p>
                                <tr>
                                    <td>
                                        <input type="file" name="berkas" onChange={this.onChangefile}/>
                                    </td>
                                </tr>
                            </div>
                            <tr>
                            <td><input type="submit" value="Submit" onClick={this.handleClick}/></td>   
                            </tr>
                        </table>
                        </Form>
                        

                    </div>
                </div>
            )
        }
    }

    handleChange = (event) => {
        const {id, value} = event.target;
        this.setState((prevState) => ({
            dataspk: {
                ...prevState.dataspk,
                [id]: value
            }
        }));
    };

}

export default (props) => {
    const params = useParams();

  
  

    return (
        <SPKADD {...props} params={params}/>
    );
}
