import React, {Component} from 'react';
import axios from 'axios';

import '../App.css'
import RecordSperjAdd from './RecordSperjAdd';


class DSperjAdd extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            datasperj: []
        })
    }

    componentDidMount() {
        var url = window.$apiBaseURL + '/get-datasperj-add';
        var token = localStorage.getItem('token');
        var config = {
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json',
                Authorization: `Bearer `+token
            },
        };
        axios.get(url, config).then(response => {
            this.setState({datasperj: response.data.results});
        })
            .catch(function (error) {
                console.log(error);
            })
    }

    usersList() {
        // console.log("asd");
        // console.log(this.state.dsurat);

        return this.state.datasperj.map(function (object, index) {
            return <RecordSperjAdd formData={object} key={index}/>;
        });
        // console.log(data);


    }


    render() {
        return (
            <div>
                <div className='DEmail'>Data SPerj ADD</div>

                <div className='Search1'>
                    <table>
                        <tr>
                            <td><input type="text" id="searchnomor" placeholder="Cari Surat"/></td>
                            <td><input type="submit" value="Search"/></td>
                        </tr>
                    </table>

                </div>
                <div className='tableemail-container'>
                    <div className='tabledashboard'>
                        <table border="1" cellspacing="0" width="1000px" height="60px">
                            <thead>
                            <th>No</th>
                            <th>PIC</th>
                            <th>Nomor SPERJ ADD</th>
                            <th>Addendum atas SPERJ No.</th>
                            <th>Tanggal Add</th>
                            <th>Nama Vendor / Tenant</th>
                            <th>Perwakilan</th>
                            <th width="500px">Perihal</th>
                            <th>Perubahan</th>
                            <th>Pajak</th>
                            <th>Tanggal Mulai</th>
                            <th>Tanggal Selesai</th>
                            <th>File</th>
                            <th>Keterangan</th>
                            <th>Action</th>

                            </thead>
                            <tbody>
                            {this.usersList()}
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

        )
    }


}

export default DSperjAdd