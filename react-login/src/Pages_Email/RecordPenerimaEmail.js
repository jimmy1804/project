import React, {Component} from "react";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faFilePdf, faPenToSquare, faTrash} from '@fortawesome/free-solid-svg-icons';
import axios from "axios";

class RecordPenerimaEmail extends Component {
    onClickDelete(e) {
        e.preventDefault();
        var id = e.currentTarget.dataset.id;

        const formData = new FormData();

        formData.append('id', id);
        var token = localStorage.getItem('token');
        var config = {
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json',
                Authorization: `Bearer ` + token
            },
        };
        var url = window.$apiBaseURL + '/delete-email';
        axios.post(url, formData, config).then((response) => {
            console.log(response.data);
            if (response.data.status) {
               window.location.reload()
            } else console.log(response.data.message);
        }).catch((error) => {
            console.log(error.response.data);
            window.location.href = "/";
        });
    }
    render() {
        
        var _delete = localStorage.getItem('_delete');
        var _edit = localStorage.getItem('_edit');
        var eedit = 'http://localhost:3000/EditPenerimaEmail/' + this.props.formData.id;
        return (
            <tr>
                <td>
                    {this.props.formData.id}
                </td>
                <td>
                    {this.props.formData.nama}
                </td>
                <td>
                    {this.props.formData.email_penerima}
                </td>
                <td>
                    {this.props.formData.gender}
                </td>
                <td>
                {_edit == 0 ? '' : <a href={eedit}><FontAwesomeIcon
                        icon={faPenToSquare} size="2x" className="fawesome"></FontAwesomeIcon></a>} 
                {_delete == 1 ? <a href="#" data-id={this.props.formData.id} onClick={this.onClickDelete}><FontAwesomeIcon
                        icon={faTrash} size="2x" className="fawesome"></FontAwesomeIcon></a> : ''}
                </td>
            </tr>
        );
        /* if(this.props.formData.data_pengirim == null){
             return(
                 <tr >
                   <td>
                   {this.props.formData.id}
                     </td>
                     <td>
                         {this.props.formData.nomoragenda}
                     </td>
                     <td>
                         {this.props.formData.asalsurat}
                     </td>
                     <td>

                        <p>tidak ada pengirim</p>
                     </td>
                     <td>
                         {this.props.formData.updated_at}
                     </td>
                 </tr>
             );
         }
         else{
             return(
                 <tr >
                   <td>
                   {this.props.formData.id}
                     </td>
                     <td>
                         {this.props.formData.nomoragenda}
                     </td>
                     <td>
                         {this.props.formData.asalsurat}
                     </td>
                     <td>

                         {this.props.formData.data_pengirim.nama}
                     </td>
                     <td>
                         {this.props.formData.updated_at}
                     </td>
                 </tr>
             );
         }
         */

    }
}

export default RecordPenerimaEmail;