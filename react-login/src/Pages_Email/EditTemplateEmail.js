import React, {Component} from 'react';
import axios from 'axios';
import Form from 'react-bootstrap/Form';
import {
    BrowserRouter as Router,
    Link,
    Route,
    Routes,
    useNavigate,
    useParams,
} from "react-router-dom";

class EditTemplateEmail extends Component {
    

    constructor(props) {
        

        super(props);
        this.state = ({
     
            template_email: []

        })

        this.handleClick = this.handleClick.bind(this);
    }
 

   


    componentDidMount() {
      
        var token = localStorage.getItem('token');
        var config = {
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json',
                Authorization: `Bearer ` + token
            },
        };
        var url = window.$apiBaseURL + '/get-templateemail/' + this.props.params.id;
        axios.get(url, config).then(response => {
           console.log(response.data);
            this.setState({template_email: response.data.results});
        })
            .catch(function (error) {
                console.log(error);
            })
    }

    handleClick() {

        var template_email = this.state.template_email;
        const formData = new FormData();

        formData.append('judul_template', template_email.judul_template);
        formData.append('title', template_email.title);
        formData.append('isi_email', template_email.isi_email);
     

           // formData.append('user_username', localStorage.getItem('user_username'));

        var token = localStorage.getItem('token');
        var config = {
            headers: {Authorization: `Bearer `+token}
        };
        const templateemail = this.props.params.id
        var url = window.$apiBaseURL + `/post-templateemail/${templateemail}`;

        axios.post(url, formData, config).then((response) => {
            alert("berhasil");
            if (response.data.status) {
            console.log(response.data);
                window.location.href="/JemailTemplate";
            }
        }).catch((error) => {
            window.location.href="/";
            console.log(error.response.data);
        });

/*
        var token = localStorage.getItem('token');
        var config = {
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json',
                Authorization: `Bearer ` + token
            },
        };
        var url = window.$apiBaseURL + '/post-composespk-adamdum';


        axios.post(url, config,formData).then((response) => {
            // console.log(response);
            // alert("berhasil");
            if (response.data.status) {


            }
        }).catch((error) => {
            console.log(error.response.data);
          
            // console.log(error, "ini error");
        });
*/
    }

    render() {
        var _edit = localStorage.getItem('_edit');
        if(_edit == 0) {
            window.location.href='/DSpk';
            return;
        } else {

            var template_email = this.state.template_email;

            return (
                <div>
                    <div className='SPKADD'>Edit Penerima Email</div>
                    <div className='FormSPKADD'>
                        <table>
                        <tr>
                            <td>Judul Template</td>
                            <td><Form.Control type="text" id="judul_template" value={template_email.judul_template}
                                              onChange={this.handleChange}/></td>
                        </tr>
                        <tr>
                            <td>Title</td>
                            <td><Form.Control type="text" id="title" value={template_email.title}
                                             onChange={this.handleChange}/></td>
                        </tr>
                        <tr>
                            <td>Isi Email</td>
                            <td><textarea type="text" id="isi_email" value={template_email.isi_email}
                                     onChange={this.handleChange} rows="10" cols="50"></textarea></td>
                           
                        </tr>
                        <tr>
                                <td><input type="submit" value="Submit" onClick={this.handleClick}/></td>
                            </tr>

                        </table>

                    </div>
                </div>
            )
        }
    }

    handleChange = (event) => {
        const {id, value} = event.target;
        this.setState((prevState) => ({
            template_email: {
                ...prevState.template_email,
                [id]: value
            }
        }));
    };

}

export default (props) => {
    const params = useParams();

  
  

    return (
        <EditTemplateEmail {...props} params={params}/>
    );
}
