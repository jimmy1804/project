import React, {Component, useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';
import RecordPenerimaEmail from './RecordPenerimaEmail';

class Jemail extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            penerima_email: []
        })
    }

    componentDidMount() {
        var url = window.$apiBaseURL + '/get-penerimaemail';
        var token = localStorage.getItem('token');
        var config = {
            headers: {Authorization: `Bearer ` + token}
        };
        axios.get(url, config).then(response => {
            this.setState({penerima_email: response.data.results});
        }).catch(function (error) {
            window.location.href = '/';
            console.log(error);
        })
    }

    usersList() {
        return this.state.penerima_email.map(function (object, index) {
            return <RecordPenerimaEmail formData={object} key={index}/>;
        });
    }


    render() {
        var _add = localStorage.getItem('_add');
        return (
            <div>
                <div className='JEmail'>Email Penerima</div>
                {_add == 1 ?
                    <div className='Search2'>
                        <table>
                            <tr>
                                <td><Link to='/Jemailt'><input type="button" value="Tambah" className='Tambah1'/></Link>
                                </td>
                            </tr>
                        </table>
                    </div>
                    : ''}

                <div className='tableemail-container'>
                    <div className='tabledashboard'>
                        <table border="1" cellspacing="0" width="1000px" height="60px">
                            <thead>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Email Penerima</th>
                            <th>Gender</th>
                            <th>Action</th>


                            </thead>
                            <tbody>
                            {this.usersList()}
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

        )
    }


}

export default Jemail