import React, {Component} from 'react';
import axios from 'axios';
import {
    BrowserRouter as Router,
    Link,
    Route,
    Routes,
    useParams,
} from "react-router-dom";

class SperjAdd extends Component {

    constructor(props) {

        super(props);
        
        this.onChangenosperjadd = this.onChangenosperjadd.bind(this);
        this.onChangetgladd = this.onChangetgladd.bind(this);
        this.onChangeperihal = this.onChangeperihal.bind(this);
        this.onChangeperubahan = this.onChangeperubahan.bind(this);
        this.onChangepajak = this.onChangepajak.bind(this);
        this.onChangetglmulai = this.onChangetglmulai.bind(this);
        this.onChangetglselesai = this.onChangetglselesai.bind(this);
        this.onChangeketerangan = this.onChangeketerangan.bind(this);
        this.onChangefile = this.onChangefile.bind(this);
        this.state = ({
            addnosperj: '',
            tgladd: '',
            perihal: '',
            perubahan: '',
            pajak : '',
            tglmulai: '',
            tglselesai: '',
            keterangan: '',
            file: '',
            datasperj: []

        })

        this.handleClick = this.handleClick.bind(this);
    }

    onChangenosperjadd(e) {
        this.setState({
            nosperjadd: e.target.value
        });
    }

    onChangetgladd(e) {
        this.setState({
            tgladd: e.target.value
        });
    }

    onChangeperihal(e) {
        this.setState({
            perihal: e.target.value
        });
    }

    onChangeperubahan(e) {
        this.setState({
            perubahan: e.target.value
        });
    }
    onChangepajak(e) {
        this.setState({
            pajak: e.target.value
        });
    }


    onChangetglmulai(e) {
        this.setState({
            tglmulai: e.target.value
        });
    }

    onChangetglselesai(e) {
        this.setState({
            tglselesai: e.target.value
        });
    }

    onChangeketerangan(e) {
        this.setState({
            tglmulai: e.target.value
        });
    }

    onChangefile(e) {
        let files = e.target.files;
        // console.log(files[0]);
        // this.setState({ file: files[0] }, () => { console.log(this.state.files) });
        this.setState({
            file: files[0]
        });
    }


    componentDidMount() {
        var token = localStorage.getItem('token');
        var config = {
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json',
                Authorization: `Bearer ` + token
            },
        };
        var url = window.$apiBaseURL + '/get-datasperj/' + this.props.params.id;
        axios.get(url, config).then(response => {
            this.setState({datasperj: response.data.results});
        })
            .catch(function (error) {
                console.log(error);
            })
    }

    handleClick() {

        var datasperj = this.state.datasperj;
        const formData = new FormData();

        formData.append('surat_awal_id', this.props.params.id);
        formData.append('pic', datasperj.pic);
        formData.append('nosperjadd', this.state.nosperjadd);
        formData.append('nomorsperj', datasperj.nomorsperj);
        formData.append('tgladd', this.state.tgladd);
        formData.append('namavendor', datasperj.namavendor);
        formData.append('perwakilan', datasperj.perwakilan);
        formData.append('perihal', this.state.perihal);
        formData.append('perubahan', this.state.perubahan);
        formData.append('pajak', this.state.pajak);
        formData.append('tglmulai', this.state.tglmulai);
        formData.append('tglselesai', this.state.tglselesai);
        formData.append('file', this.state.file);
        formData.append('keterangan', this.state.keterangan);


        var token = localStorage.getItem('token');
        var config = {
            headers: {Authorization: `Bearer `+token}
        };
        var url = window.$apiBaseURL + '/post-composesperj-add';

        axios.post(url, formData, config).then((response) => {
            alert("berhasil");
            if (response.data.status) {
                window.location.href="DSperjAdd";
            }
        }).catch((error) => {
            window.location.href="/";
            console.log(error.response.data);
        });

        /*
        var url = window.$apiBaseURL + '/post-composesperj-add';
        axios.post(url, formData).then((response) => {
            // console.log(response);
            // alert("berhasil");
            if (response.data.status) {


            }
        }).catch((error) => {
            console.log(error.response.data);
            // console.log(error, "ini error");
        });
        */
    }

    render() {
        

        var datasperj = this.state.datasperj;

        return (
            
            <div>
                <div className='SPKADD'>SPK ADD untuk surat id ke</div>
                <div className='FormSPKADD'>
                    <table>
                    <tr>
                                <td>PIC</td>

                                <td><input type="text" id="pic" value={datasperj.pic}
                                           onChange={this.handleChange}/></td>
                            </tr>
                        <tr>
                            <td>Nomor SPERJ ADD</td>
                            <td ><input type="text" id="nosperjadd" value={this.state.nosperjadd}
                                       onChange={this.onChangenosperjadd}/></td>
                        </tr>
                        <tr>
                            <td>Addendum atas SPERJ No.</td>
                            <td><input type="text" id="nomorsperj" value={datasperj.nomorsperj}
                                       onChange={this.handleChange}/></td>
                        </tr>
                        <tr>
                            <td>Tgl ADD</td>
                            <td><input type="text" id="tgladd" value={this.state.tgladd}
                                       onChange={this.onChangetgladd}/></td>
                        </tr>
                        <tr>
                            <td>Nama Vendor / Kontraktor</td>

                            <td><input type="text" id="namavendor" value={datasperj.namavendor}
                                       onChange={this.handleChange}/></td>
                        </tr>
                        <tr>
                            <td>Perwakilan (dasar kewewenangan)</td>
                            <td><input type="text" id="perwakilan" value={datasperj.perwakilan}
                                       onChange={this.handleChange}/></td>
                        </tr>
                        <tr>
                            <td>Perihal</td>
                            <td><input type="text" id="perihal" value={this.state.perihal}
                                       onChange={this.onChangeperihal}/></td>
                        </tr>
                        <tr>
                            <td>Perubahan</td>
                            <td><input type="text" id="perubahan" value={this.state.perubahan}
                                       onChange={this.onChangeperubahan}/></td>
                        </tr>
                        <tr>
                            <td>Pajak</td>
                            <td><input type="text" id="pajak" value={this.state.pajak}
                                       onChange={this.onChangepajak}/></td>
                        </tr>
                        <tr>
                            <td>Tanggal Mulai</td>
                            <td><input type="text" id="tglmulai" value={this.state.tglmulai}
                                       onChange={this.onChangetglmulai}/></td>
                        </tr>
                        <tr>
                            <td>Tanggal Selesai</td>
                            <td><input type="text" id="tglselesai" value={this.state.tglselesai}
                                       onChange={this.onChangetglselesai}/></td>
                        </tr>
                        <tr>
                            <td>Keterangan</td>
                            <td><textarea id="keterangan" value={this.state.keterangan}
                                          onChange={this.onChangeketerangan}></textarea></td>
                        </tr>
                        <div className='lampiran1'>
                            <p>Lampiran</p>
                            <tr>
                                <td>
                                    <input type="file" name="berkas" onChange={this.onChangefile}/>
                                </td>
                            </tr>
                        </div>
                        <tr>
                            <td><input type="submit" value="Submit" onClick={this.handleClick}/></td>
                        </tr>
                    </table>

                </div>
            </div>
        )

    }

    handleChange = (event) => {
        const {id, value} = event.target;
        this.setState((prevState) => ({
            datasperj: {
                ...prevState.datasperj,
                [id]: value
            }
        }));


    };

}

export default (props) => {
    const params = useParams();

    return (
        <SperjAdd {...props} params={params}/>
    );
}
