import React, { Component } from 'react'

export default class TArsip extends Component {
    constructor(props){
        super(props);
        this.onChangeNomorAgenda = this.onChangeNomorAgenda.bind(this);
        this.onChangeNomorSurat = this.onChangeNomorSurat.bind(this);
        this.onChangeKeterangan = this.onChangeKeterangan.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            NomorAgenda: '',
            NomorSurat: '',
            Keterangan: ''
        }
  }

  onChangeNomorAgenda(e){
      this.setState({
          NomorAgenda: e.target.value
      });
  }
  onChangeNomorSurat(e){
    this.setState({
        NomorSurat: e.target.value
    });
}
onChangeKeterangan(e){
    this.setState({
        Keterangan: e.target.value
    });
}
onSubmit(e){
    e.preventDefault();

    const obj = {
        NomorAgenda: this.state.NomorAgenda,
        NomorSurat: this.state.NomorAgenda,
        Keterangan: this.state.Keterangan
    }
    console.log(obj)
}
  render() {
    return (
        <div>
        <div className='TArsip'>Tambah Arsip</div>
        <div className='FormC'>
            <form onSubmit={this.onSubmit}>
        <table>
            <tr>
                <td>Nomor Agenda</td>
                <td><input type="text" id="nomoragenda"
                value={this.state.NomorAgenda} onChange={this.onChangeNomorAgenda}/></td>
            </tr>
            <tr>
                <td>Nomor Surat</td>
                <td><input type="text" id="nomorsurat"
                value={this.state.NomorSurat} onChange={this.onChangeNomorSurat}/></td>
            </tr>
            <tr>
                <td>Keterangan</td>
                <td><textarea 
                value={this.state.Keterangan} onChange={this.onChangeKeterangan}></textarea></td>
            </tr>
            <div className='lampiran1'>
            <p>Lampiran</p>
            <tr> 
            <td>
            <input type="file" name="berkas" />
            </td>
            <td><input type="button" name="upload" value="Upload" className='upload1' /> </td>
            </tr>
            </div>
            <tr>
                <td><input type="submit" value="Submit" /></td>
            </tr>
        </table>
        </form>
        </div>
    </div>
    )
  }
}
