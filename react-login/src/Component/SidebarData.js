import React from "react";
import * as FaIcons from 'react-icons/fa'
import * as AiIcons from 'react-icons/ai'
import * as IoIcons from 'react-icons/io'
import * as RiIcons from 'react-icons/ri'
import * as GrIcons from 'react-icons/gr'
import * as MdIcons from 'react-icons/md'
import * as GoIcons from 'react-icons/go'

export const SidebarData = [
    {
        title: 'Dashboard',
        path: '/Dashboard',
        icon: <AiIcons.AiFillHome/>
    },
    {
        title: 'Surat',
        path: '#',
        icon: <GrIcons.GrMail/>,
        iconClosed: <RiIcons.RiArrowDownSFill/>,
        iconOpened: <RiIcons.RiArrowUpSFill/>,
        subNav: [
            {
                title: 'Compose',
                path: '/SuratC',
                icon: <MdIcons.MdDoubleArrow/>,
            },
            {
                title: 'Data Surat',
                path: '/DSurat',
                icon: <MdIcons.MdDoubleArrow/>,
            }
        ]
    },
    {
        title: 'Arsip',
        path: '#',
        icon: <FaIcons.FaArchive/>,
        iconClosed: <RiIcons.RiArrowDownSFill/>,
        iconOpened: <RiIcons.RiArrowUpSFill/>,
        subNav: [
            {
                title: 'Arsip Saya',
                path: '/ArsipS',
                icon: <MdIcons.MdDoubleArrow/>,
            },
            {
                title: 'Tambah Arsip',
                path: '/TArsip',
                icon: <MdIcons.MdDoubleArrow/>,
            },
            {
                title: 'Data Arsip',
                path: '/DArsip',
                icon: <MdIcons.MdDoubleArrow/>,
            },
            {
                title: 'History Lihat',
                path: '/HLihat',
                icon: <MdIcons.MdDoubleArrow/>,
            },
            {
                title: 'History Unduh',
                path: '/HUnduh',
                icon: <MdIcons.MdDoubleArrow/>,
            }
        ]
    },
    {
        title: 'Email',
        path: '#',
        icon: <MdIcons.MdAlternateEmail/>,
        iconClosed: <RiIcons.RiArrowDownSFill/>,
        iconOpened: <RiIcons.RiArrowUpSFill/>,
        subNav: [
        
            {
                title: 'SPK / Email',
                path: '/SPK',
                icon: <MdIcons.MdDoubleArrow/>,
            },
            {
                title: 'SPerj',
                path: '/SPerj',
                icon: <MdIcons.MdDoubleArrow/>,
            },
    
            {
                title: 'Data SPK',
                path: '/DSpk',
                icon: <MdIcons.MdDoubleArrow/>,
            },
            {
                title: 'Data SPK ADD',
                path: '/DSpkAdd',
                icon: <MdIcons.MdDoubleArrow/>,
            },
            {
                title: 'Data SPerj',
                path: '/DSperj',
                icon: <MdIcons.MdDoubleArrow/>,
            },
            {
                title: 'Data Sperj Add',
                path: '/DSperjAdd',
                icon: <MdIcons.MdDoubleArrow/>,
            },
            {
                title: 'Penerima Email',
                path: '/JEmail',
                icon: <MdIcons.MdDoubleArrow/>,
            },
            {
                title: 'Template Isi Email ',
                path: '/JEmailTemplate',
                icon: <MdIcons.MdDoubleArrow/>,
            },
            {
                title: 'Kirim',
                path: '/Kirim',
                icon: <MdIcons.MdDoubleArrow/>,
            }
        ]
    },
    {
        title: 'Menu Akun',
        path: '/MenuAkun',
        icon: <FaIcons.FaUserCog/>,
    }
]