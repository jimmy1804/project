import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import * as FaIcons from 'react-icons/fa'
import * as AiIcons from 'react-icons/ai'
import { SidebarData } from './SidebarData'
import SubMenu from './SubMenu'
import './Sidebar.css'

const Nav = styled.nav`
padding-top: 10px;
height: 110px;
width: 200px;
background-color: black;
`;


const Sidebar = () => {
  return (
    <div className='Sidebar'>
      <div className='SidebarMenu'>
      <Nav>
        <img src={require('../Img/yayasanuntar.png')} className='Logo'/>
      </Nav>
      {SidebarData.map((item, index) => {
        return <SubMenu item={item} key={index}/>
      })}
    </div>  
    </div>
  )
}

export default Sidebar