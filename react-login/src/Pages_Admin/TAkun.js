import React, { Component } from 'react'
import axios from 'axios';

export default class TAkun extends Component {
    constructor(props){
        super(props);
        this.onChangeUsername = this.onChangeUsername.bind(this);
        this.onChangeNama = this.onChangeNama.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangeDivisi = this.onChangeDivisi.bind(this);
        this.onChangeUnit = this.onChangeUnit.bind(this);
        this.onChangeAkses = this.onChangeAkses.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            Username: '',
            Nama: '',
            Password: '',
            Email: '',
            Divisi:'',
            Unit: '',
            Akses: ''
        }
  }
  onChangeUsername(e){
    this.setState({
        Username: e.target.value
    });
}
onChangeNama(e){
  this.setState({
      Nama: e.target.value
  });
}
onChangePassword(e){
  this.setState({
      Password: e.target.value
  });
}
onChangeEmail(e){
  this.setState({
      Email: e.target.value
  });
}
onChangeDivisi(e){
  this.setState({
      Divisi: e.target.value
  });
}
onChangeUnit(e){
  this.setState({
      Unit: e.target.value
  });
}
onChangeAkses(e){
  this.setState({
      Akses: e.target.value
  });
}
onSubmit(e){
  e.preventDefault();

  const obj = {
      Username: this.state.Username,
      Nama: this.state.Nama,
      Password: this.state.Password,
      Email: this.state.Email,
      Divisi: this.state.Divisi,
      Unit: this.state.Unit,
      Akses: this.state.Akses
  }
  //console.log(obj)
  axios.post("https://localhost/Rest/TAkunAd.php",obj)
  .then(res => console.log(res.data));
}

  render() {
    return (
        <div>
        <div className='TAkun'>Tambah Akun</div>
        <div className='tambah-akun'>

        <table>
            <tr>
                <td>Username</td>
                <td><input type="text" id="username"/></td>
            </tr>
            <tr>
                <td>Nama</td>
                <td><input type="text" id="nama"/></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="password" id="password"/></td>
            </tr>
            <tr>
                <td>Email</td>
                <td><input type="text" id="email"/></td>
            </tr>
            <tr>
                <td>Divisi</td>
                <td><input type="text" id="divisi"/></td>
            </tr>
            <tr>
                <td>Unit</td>
                <td><input type="text" id="unit"/></td>
            </tr>
            <tr>
                <td>Akses</td>
                <td><input type="text" id="akses"/></td>
            </tr>
            <tr>
                <td><input type="submit" value="Submit" /></td>
            </tr>
        </table>

        </div>   
    </div>
    )
  }
}
