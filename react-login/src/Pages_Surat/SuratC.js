import React, { Component } from 'react'
import axios from 'axios';

export default class SuratC extends Component {
    constructor(props){
        super(props);
        this.onChangeNomorAgenda = this.onChangeNomorAgenda.bind(this);
        this.onChangeAsalSurat = this.onChangeAsalSurat.bind(this);
        this.onChangeTujuanSurat = this.onChangeTujuanSurat.bind(this);
        this.onChangeNomorSurat = this.onChangeNomorSurat.bind(this);
        this.onChangeTanggalSurat = this.onChangeTanggalSurat.bind(this);
        this.onChangePerihal = this.onChangePerihal.bind(this);
        this.onChangeKeterangan = this.onChangeKeterangan.bind(this);
        this.handleClick = this.handleClick.bind(this);

        this.state = {
            NomorAgenda: '',
            AsalSurat: '',
            TujuanSurat: '',
            NomorSurat: '',
            TanggalSurat:'',
            Perihal: '',
            Keterangan: ''
        }
  }

  onChangeNomorAgenda(e){
      this.setState({
          NomorAgenda: e.target.value
      });
  }
  onChangeAsalSurat(e){
    this.setState({
        AsalSurat: e.target.value
    });
}
onChangeTujuanSurat(e){
    this.setState({
        TujuanSurat: e.target.value
    });
}
onChangeNomorSurat(e){
    this.setState({
        NomorSurat: e.target.value
    });
}
onChangeTanggalSurat(e){
    this.setState({
        TanggalSurat: e.target.value
    });
}
onChangePerihal(e){
    this.setState({
        Perihal: e.target.value
    });
}
onChangeKeterangan(e){
    this.setState({
        Keterangan: e.target.value
    });
}
handleClick(e){
    e.preventDefault();

    const obj = {
        NomorAgenda: this.state.NomorAgenda,
        AsalSurat: this.state.AsalSurat,
        TujuanSurat: this.state.TujuanSurat,
        NomorSurat: this.state.NomorSurat,
        TanggalSurat: this.state.TanggalSurat,
        Perihal: this.state.Perihal,
        Keterangan: this.state.Keterangan
    }
   // console.log(obj)
    axios.post("http://localhost/backend/public/_api/post-compose", obj)
    .then(res=>console.log(res.data))
}
  render() {
    return (
        <div>
        <div className='SuratC'>Compose</div>
        <div className='FormC'>
            
            <table>
            <tr>
                <td>Nomor Agenda</td>
                <td><input type="text" id="nomoragenda"
                value={this.state.NomorAgenda} onChange={this.onChangeNomorAgenda}/></td>
            </tr>
            <tr>
                <td>Asal Surat</td>
                <td><input type="text" id="asalsurat"
                value={this.state.AsalSurat} onChange={this.onChangeAsalSurat}/></td>
            </tr>
            <tr>
                <td>Tujuan Surat</td>
                <td><input type="text" id="tujuansurat"
                value={this.state.TujuanSurat} onChange={this.onChangeTujuanSurat}/></td>
            </tr>            <tr>
                <td>Nomor Surat</td>
                <td><input type="text" id="nomorsurat"
                value={this.state.NomorSurat} onChange={this.onChangeNomorSurat}/></td>
                <td>Tanggal Surat</td>
                <td><input type="date" id="tanggalsurat"
                value={this.state.TanggalSurat} onChange={this.onChangeTanggalSurat}/></td>
            </tr>
            <tr>
                <td>Perihal</td>
                <td><textarea 
                value={this.state.Perihal} onChange={this.onChangePerihal}></textarea></td>
            </tr>
            <tr>
                <td>Keterangan</td>
                <td><input type="text" id="keterangan"
                value={this.state.Keterangan} onChange={this.onChangeKeterangan}/></td>
            </tr>
            <div className='lampiran1'>
            <p>Lampiran</p>
            <tr> 
            <td>
            <input type="file" name="berkas" />
            </td>
            <td><input type="submit" name="upload" value="Upload" className='upload1' /> </td>
            </tr>
            </div>
            <tr>
               <button onClick={this.handleClick}>submit</button>
            </tr>
        </table>
        
        </div>
    </div>
    )
  }
}
